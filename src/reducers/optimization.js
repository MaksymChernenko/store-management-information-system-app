import * as R from 'ramda';

import { SUCCESS } from '../services/reduxHelpers';

import * as actions from '../actions/optimization';

export const ROOT = 'optimization';

const initialState = {
  productProfit: null,
  profitRetentionCurveData: [],
};

const optimizationReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.CALCULATE_PRODUCT_PROFIT[SUCCESS]:
      return R.mergeDeepRight(state, {
        productProfit: data.result,
      });
    case actions.BUILD_PROFIT_RETENTION_CURVE[SUCCESS]:
      return R.mergeDeepRight(state, {
        profitRetentionCurveData: data.result,
      });
    default:
      return state;
  }
};

export default optimizationReducer;
