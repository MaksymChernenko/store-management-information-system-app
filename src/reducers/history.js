import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../services/reduxHelpers';

import * as actions from '../actions/history';

export const ROOT = 'history';

const initialState = {
  notes: {
    data: [],
    state: null,
    filters: {
      user: '',
    },
  },
};

const historyReducer = (state = initialState, { type, data, payload }) => {
  switch (type) {
    case actions.FETCH_NOTES[START]:
      return R.mergeDeepRight(state, {
        notes: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_NOTES[SUCCESS]:
      return R.mergeDeepRight(state, {
        notes: {
          data: data.notes,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_NOTES[ERROR]:
      return R.mergeDeepRight(state, {
        notes: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.SET_NOTES_FILTERS:
      return R.mergeDeepRight(state, {
        notes: {
          filters: {
            user: payload.user,
          },
        },
      });
    case actions.CLEAR_NOTES_FILTERS:
      return R.mergeDeepRight(state, {
        notes: {
          filters: {
            user: '',
          },
        },
      });

    default:
      return state;
  }
};

export default historyReducer;
