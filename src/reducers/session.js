import * as R from 'ramda';

import { SUCCESS, ERROR } from '../services/reduxHelpers';

import * as actions from '../actions/session';

export const ROOT = 'session';

const initialState = {
  isAuthenticated: false,
  isRefreshed: false,
  user: null,
  token: '',
  users: [],
};

const sessionReducer = (state = initialState, { type, data, payload }) => {
  switch (type) {
    case actions.SIGN_UP[SUCCESS]:
    case actions.SIGN_IN[SUCCESS]:
      return R.mergeDeepRight(state, {
        isAuthenticated: true,
        isRefreshed: true,
        user: data.user,
        token: data.token,
      });
    case actions.REFRESH_USER[SUCCESS]:
      return R.mergeDeepRight(state, {
        isAuthenticated: true,
        isRefreshed: true,
        user: data.user,
      });
    case actions.SIGN_UP[ERROR]:
    case actions.SIGN_IN[ERROR]:
    case actions.REFRESH_USER[ERROR]:
    case actions.SIGN_OUT[SUCCESS]:
      return R.mergeDeepRight(state, {
        isAuthenticated: false,
        isRefreshed: true,
        user: null,
        token: '',
      });
    case actions.SET_IS_REFRESHED:
      return R.mergeDeepRight(state, { isRefreshed: payload });
    case actions.FETCH_USERS[SUCCESS]:
      return R.mergeDeepRight(state, {
        users: data.users,
      });
    case actions.ADD_STORE_TO_USER[SUCCESS]:
      if (payload.withoutUpdate) return state;
      return R.mergeDeepRight(state, {
        user: data.user,
      });
    default:
      return state;
  }
};

export default sessionReducer;
