import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/waybill';

export const ROOT = 'waybill';

const initialState = {
  waybills: {
    data: [],
    state: null,
  },
};

const waybillReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_WAYBILLS[START]:
      return R.mergeDeepRight(state, {
        waybills: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_WAYBILLS[SUCCESS]:
      return R.mergeDeepRight(state, {
        waybills: {
          data: data.waybills,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_WAYBILLS[ERROR]:
      return R.mergeDeepRight(state, {
        waybills: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_WAYBILLS:
      return R.mergeDeepRight(state, {
        waybills: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default waybillReducer;
