import { combineReducers } from 'redux';

import positionReducer, { ROOT as POSITION_ROOT } from './position';
import employeeReducer, { ROOT as EMPLOYEE_ROOT } from './employee';
import productReducer, { ROOT as PRODUCT_ROOT } from './product';
import saleReducer, { ROOT as SALE_ROOT } from './sale';
import supplierReducer, { ROOT as SUPPLIER_ROOT } from './supplier';
import waybillReducer, { ROOT as WAYBILL_ROOT } from './waybill';

export const ROOT = 'entries';

const entriesReducer = combineReducers({
  [POSITION_ROOT]: positionReducer,
  [EMPLOYEE_ROOT]: employeeReducer,
  [PRODUCT_ROOT]: productReducer,
  [SALE_ROOT]: saleReducer,
  [SUPPLIER_ROOT]: supplierReducer,
  [WAYBILL_ROOT]: waybillReducer,
});

export default entriesReducer;
