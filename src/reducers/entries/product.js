import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/product';

export const ROOT = 'product';

const initialState = {
  products: {
    data: [],
    state: null,
  },
};

const productReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_PRODUCTS[START]:
      return R.mergeDeepRight(state, {
        products: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_PRODUCTS[SUCCESS]:
      return R.mergeDeepRight(state, {
        products: {
          data: data.products,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_PRODUCTS[ERROR]:
      return R.mergeDeepRight(state, {
        products: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_PRODUCTS:
      return R.mergeDeepRight(state, {
        products: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default productReducer;
