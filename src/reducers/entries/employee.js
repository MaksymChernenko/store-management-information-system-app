import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/employee';

export const ROOT = 'employee';

const initialState = {
  employees: {
    data: [],
    state: null,
  },
};

const employeeReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_EMPLOYEES[START]:
      return R.mergeDeepRight(state, {
        employees: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_EMPLOYEES[SUCCESS]:
      return R.mergeDeepRight(state, {
        employees: {
          data: data.employees,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_EMPLOYEES[ERROR]:
      return R.mergeDeepRight(state, {
        employees: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_EMPLOYEES:
      return R.mergeDeepRight(state, {
        employees: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default employeeReducer;
