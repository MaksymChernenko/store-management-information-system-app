import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/supplier';

export const ROOT = 'supplier';

const initialState = {
  suppliers: {
    data: [],
    state: null,
  },
};

const supplierReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_SUPPLIERS[START]:
      return R.mergeDeepRight(state, {
        suppliers: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_SUPPLIERS[SUCCESS]:
      return R.mergeDeepRight(state, {
        suppliers: {
          data: data.suppliers,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_SUPPLIERS[ERROR]:
      return R.mergeDeepRight(state, {
        suppliers: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_SUPPLIERS:
      return R.mergeDeepRight(state, {
        suppliers: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default supplierReducer;
