import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/position';

export const ROOT = 'position';

const initialState = {
  positions: {
    data: [],
    state: null,
  },
};

const positionReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_POSITIONS[START]:
      return R.mergeDeepRight(state, {
        positions: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_POSITIONS[SUCCESS]:
      return R.mergeDeepRight(state, {
        positions: {
          data: data.positions,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_POSITIONS[ERROR]:
      return R.mergeDeepRight(state, {
        positions: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_POSITIONS:
      return R.mergeDeepRight(state, {
        positions: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default positionReducer;
