import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../../services/reduxHelpers';

import * as actions from '../../actions/sale';

export const ROOT = 'sale';

const initialState = {
  sales: {
    data: [],
    state: null,
  },
};

const saleReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_SALES[START]:
      return R.mergeDeepRight(state, {
        sales: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_SALES[SUCCESS]:
      return R.mergeDeepRight(state, {
        sales: {
          data: data.sales,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_SALES[ERROR]:
      return R.mergeDeepRight(state, {
        sales: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_SALES:
      return R.mergeDeepRight(state, {
        sales: {
          data: [],
          state: null,
        },
      });

    default:
      return state;
  }
};

export default saleReducer;
