import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../services/reduxHelpers';

import * as actions from '../actions/menuConfig';

export const ROOT = 'menu-config';

const initialState = {
  storeMenu: {
    data: null,
    state: null,
  },
  analysisMenu: {
    data: null,
    state: null,
  },
};

const menuConfigReducer = (
  state = initialState,
  { type, configType, data },
) => {
  switch (type) {
    case actions.FETCH_MENU_CONFIG[START]:
    case actions.ADD_MENU_CONFIG[START]:
      return R.mergeDeepRight(state, {
        [configType]: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_MENU_CONFIG[SUCCESS]:
    case actions.ADD_MENU_CONFIG[SUCCESS]:
      return R.mergeDeepRight(state, {
        [configType]: {
          data: data.menuConfig,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_MENU_CONFIG[ERROR]:
    case actions.ADD_MENU_CONFIG[ERROR]:
      return R.mergeDeepRight(state, {
        [configType]: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_MENU_CONFIG:
      return R.mergeDeepRight(state, {
        [configType]: {
          data: null,
          state: null,
        },
      });

    default:
      return state;
  }
};

export default menuConfigReducer;
