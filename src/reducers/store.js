import * as R from 'ramda';

import {
  START,
  SUCCESS,
  ERROR,
  FETCHING_STATE,
} from '../services/reduxHelpers';

import * as actions from '../actions/store';

export const ROOT = 'store';

const initialState = {
  stores: {
    data: [],
    state: null,
  },
  store: {
    data: null,
    state: null,
  },
};

const storeReducer = (state = initialState, { type, data }) => {
  switch (type) {
    case actions.FETCH_STORES[START]:
      return R.mergeDeepRight(state, {
        stores: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_STORES[SUCCESS]:
      return R.mergeDeepRight(state, {
        stores: {
          data: data.stores,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_STORES[ERROR]:
      return R.mergeDeepRight(state, {
        stores: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.FETCH_STORE[START]:
      return R.mergeDeepRight(state, {
        store: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_STORE[SUCCESS]:
      return R.mergeDeepRight(state, {
        store: {
          data: data.store,
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_STORE[ERROR]:
      return R.mergeDeepRight(state, {
        store: {
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.RESET_STORE:
      return R.mergeDeepRight(state, {
        store: {
          data: null,
          state: null,
        },
      });
    default:
      return state;
  }
};

export default storeReducer;
