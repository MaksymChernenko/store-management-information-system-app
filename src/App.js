import React, { useEffect } from 'react';
import Normalize from 'react-normalize';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import getRoutes from './routes';

import { refreshUser } from './actions/session';

const splashScreen = document.getElementById('splash-screen');

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    splashScreen.remove();
    dispatch(refreshUser());
  }, []);

  return (
    <App.Wrapper>
      <Normalize />
      <App.Container>{getRoutes()}</App.Container>
    </App.Wrapper>
  );
};

App.Wrapper = styled.div`
  font-family: 'Nunito', sans-serif;
`;

App.Container = styled.div`
  background-color: #fafcff;
  box-shadow: 0 0 1px;
`;

export default App;
