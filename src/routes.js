import React from 'react';
import Loadable from 'react-loadable';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import { getRefreshStatus, getAuthStatus } from './selectors/session';

import { Main, PageLoader, PrivateRoute, Store } from './components';

const SignIn = Loadable({
  loader: () => import('./pages/SignIn'),
  loading: PageLoader,
});

const SignUp = Loadable({
  loader: () => import('./pages/SignUp'),
  loading: PageLoader,
});

const SelectStore = Loadable({
  loader: () => import('./pages/SelectStore'),
  loading: PageLoader,
});

const CreateStore = Loadable({
  loader: () => import('./pages/CreateStore'),
  loading: PageLoader,
});

const StoreMenu = Loadable({
  loader: () => import('./pages/StoreMenu'),
  loading: PageLoader,
});

const EntriesConfig = Loadable({
  loader: () => import('./pages/EntriesConfig'),
  loading: PageLoader,
});

const StoreLifecycle = Loadable({
  loader: () => import('./pages/StoreLifecycle'),
  loading: PageLoader,
});

const StoreAnalysis = Loadable({
  loader: () => import('./pages/StoreAnalysis'),
  loading: PageLoader,
});

const RevenueOptimization = Loadable({
  loader: () => import('./pages/RevenueOptimization'),
  loading: PageLoader,
});

const PermissionDenied = Loadable({
  loader: () => import('./pages/PermissionDenied'),
  loading: PageLoader,
});

export const routePaths = {
  signIn: '/sign-in',
  signUp: '/sign-up',
  selectStore: '/app/select-store',
  createStore: '/app/create-store',
  permissionDenied: '/app/permission-denied',
  storeMenu: '/app/store/:id/store-menu',
  entriesConfig: '/app/store/:id/entries-config',
  storeLifecycle: '/app/store/:id/store-lifecycle',
  storeAnalysis: '/app/store/:id/store-analysis',
  revenueOptimization: '/app/store/:id/revenue-optimization',
};

const getRoutes = () => {
  const isRefreshed = useSelector(getRefreshStatus);
  const authStatus = useSelector(getAuthStatus);

  return (
    <Switch>
      <Route
        exact
        path={routePaths.signIn}
        render={props =>
          !authStatus ? <SignIn {...props} /> : <Redirect to="/app" />
        }
      />
      <Route
        exact
        path={routePaths.signUp}
        render={props =>
          !authStatus ? <SignUp {...props} /> : <Redirect to="/app" />
        }
      />
      {isRefreshed && (
        <PrivateRoute
          path="/app"
          render={props => (
            <Main {...props}>
              <Switch>
                <Route
                  exact
                  path={routePaths.selectStore}
                  component={SelectStore}
                />
                <Route
                  exact
                  path={routePaths.createStore}
                  component={CreateStore}
                />
                <Route
                  exact
                  path={routePaths.permissionDenied}
                  component={PermissionDenied}
                />
                <Route
                  path="/app/store/:id"
                  render={pr => (
                    <Store {...pr}>
                      <Switch>
                        <Route
                          exact
                          path={routePaths.storeMenu}
                          component={StoreMenu}
                        />
                        <Route
                          exact
                          path={routePaths.entriesConfig}
                          component={EntriesConfig}
                        />
                        <Route
                          exact
                          path={routePaths.storeLifecycle}
                          component={StoreLifecycle}
                        />
                        <Route
                          exact
                          path={routePaths.storeAnalysis}
                          component={StoreAnalysis}
                        />
                        <Route
                          exact
                          path={routePaths.revenueOptimization}
                          component={RevenueOptimization}
                        />
                        <Redirect to={routePaths.selectStore} />
                      </Switch>
                    </Store>
                  )}
                />
                <Redirect to={routePaths.selectStore} />
              </Switch>
            </Main>
          )}
        />
      )}
      {isRefreshed && <Redirect to={routePaths.signIn} />}
    </Switch>
  );
};

export default getRoutes;
