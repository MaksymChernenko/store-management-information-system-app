import PropTypes from 'prop-types';

import React from 'react';
import ReactSelect from 'react-select';

const getCustomStyles = (width, menuWidth) => ({
  option: state => ({
    ...state,
    cursor: 'pointer',
  }),
  menu: provided => ({
    ...provided,
    width: menuWidth || '100%',
  }),
  control: provided => ({
    ...provided,
    width: width || 300,
    marginRight: 20,
  }),
});

const Select = ({ options, onChange, value, width, menuWidth, ...props }) => (
  <ReactSelect
    styles={getCustomStyles(width, menuWidth)}
    isClearable
    isSearchable
    value={value}
    onChange={onChange}
    options={options}
    theme={theme => ({
      ...theme,
      borderRadius: 0,
      colors: {
        ...theme.colors,
        primary: 'black',
      },
    })}
    {...props}
  />
);

Select.propTypes = {
  options: PropTypes.array.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  width: PropTypes.number,
  menuWidth: PropTypes.number,
};

export default Select;
