import PropTypes from 'prop-types';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham-dark.css';

import React from 'react';
import styled from 'styled-components';

const Grid = ({
  columnDefs,
  handleRowValueChanged = () => {},
  rowData,
  style = {},
  ...props
}) => (
  <Grid.Wrapper style={style} className="ag-theme-balham-dark">
    <AgGridReact
      columnDefs={columnDefs}
      rowData={rowData}
      enableColResize
      enableFilter
      enableSorting
      suppressClickEdit
      onRowValueChanged={handleRowValueChanged}
      editType="fullRow"
      onGridReady={({ api }) => api.sizeColumnsToFit()}
      {...props}
    />
  </Grid.Wrapper>
);

Grid.propTypes = {
  columnDefs: PropTypes.array.isRequired,
  handleRowValueChanged: PropTypes.func.isRequired,
  rowData: PropTypes.array.isRequired,
  style: PropTypes.object.isRequired,
};

Grid.Wrapper = styled.div`
  height: 316px;
  filter: brightness(1.2);
`;

export default Grid;
