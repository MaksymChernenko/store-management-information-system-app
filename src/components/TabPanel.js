import PropTypes from 'prop-types';

import { Box, Typography } from '@material-ui/core';

import React from 'react';

const TabPanel = props => {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      style={{ width: '100%' }}
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.any.isRequired,
  value: PropTypes.isRequired,
};

export default TabPanel;
