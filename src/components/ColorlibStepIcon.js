import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { Money, Settings, Timeline } from '@material-ui/icons';

import React from 'react';
import clsx from 'clsx';

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor: '#373a40',
    boxShadow: '0 0 8px #767b85',
  },
  completed: {
    backgroundColor: '#373a40',
  },
});

const ColorlibStepIcon = ({ active, completed, icon }) => {
  const classes = useColorlibStepIconStyles();

  const icons = {
    1: <Settings />,
    2: <Money />,
    3: <Timeline />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(icon)]}
    </div>
  );
};

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
};

export default ColorlibStepIcon;
