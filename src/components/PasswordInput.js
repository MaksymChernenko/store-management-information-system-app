import PropTypes from 'prop-types';

import {
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Visibility, VisibilityOff, VpnKey } from '@material-ui/icons';

import React, { useState } from 'react';

const useStyles = makeStyles(() => ({
  input: {
    '& .MuiInput-underline:after': {
      borderBottom: '2px solid #292b2f',
    },
    '& .MuiFormLabel-root.Mui-focused': {
      color: '#292b2f',
    },
  },
}));

const PasswordInput = ({
  error = false,
  inputStyle = {},
  name,
  onInput,
  style = {},
  ...props
}) => {
  const classes = useStyles();

  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => setShowPassword(prev => !prev);

  return (
    <FormControl style={style} error={error}>
      <TextField
        error={error}
        autoComplete="off"
        className={classes.input}
        type={showPassword ? 'text' : 'password'}
        onChange={({ target: { value } }) => onInput(value)}
        style={inputStyle}
        name={name}
        id={name}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKey />
            </InputAdornment>
          ),
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={togglePasswordVisibility}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
        {...props}
      />
      {error && <FormHelperText id={name}>Error</FormHelperText>}
    </FormControl>
  );
};

PasswordInput.propTypes = {
  error: PropTypes.bool.isRequired,
  inputStyle: PropTypes.objectOf(PropTypes.any).isRequired,
  name: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
  style: PropTypes.PropTypes.objectOf(PropTypes.any).isRequired,
};

export default PasswordInput;
