import PropTypes from 'prop-types';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';

import React from 'react';
import { confirmable, createConfirmation } from 'react-confirm';

const ConfirmationDialog = ({ show, proceed, confirmation, options }) => (
  <Dialog
    disableBackdropClick
    disableEscapeKeyDown
    maxWidth="xs"
    aria-labelledby="confirmation-dialog-title"
    open={show}
  >
    <DialogTitle id="confirmation-dialog-title">{options.action}</DialogTitle>
    <DialogContent style={{ width: 300 }}>{confirmation}</DialogContent>
    <DialogActions>
      <Button autoFocus onClick={() => proceed(false)} color="primary">
        Cancel
      </Button>
      <Button color="primary" onClick={() => proceed(true)}>
        Ok
      </Button>
    </DialogActions>
  </Dialog>
);

ConfirmationDialog.propTypes = {
  show: PropTypes.bool,
  proceed: PropTypes.func,
  confirmation: PropTypes.string,
  options: PropTypes.object,
};

const confirm = createConfirmation(confirmable(ConfirmationDialog));

const confirmWrapper = (confirmation, options = {}) =>
  confirm({ confirmation, options });

export default confirmWrapper;
