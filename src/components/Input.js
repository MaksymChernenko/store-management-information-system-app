import PropTypes from 'prop-types';

import { InputAdornment, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import React from 'react';
import InputMask from 'react-input-mask';

const useStyles = makeStyles(() => ({
  input: {
    '& .MuiInput-underline:after': {
      borderBottom: '2px solid #292b2f !important',
    },
    '& .MuiFormLabel-root.Mui-focused': {
      color: '#292b2f !important',
    },
  },
}));

const Input = ({ icon: Icon, onInput, value: inputValue, mask, ...props }) => {
  const classes = useStyles();

  return mask === 'mobilePhone' ? (
    <InputMask
      mask="+ (999) 99 999 99 99"
      value={inputValue}
      onChange={({ target: { value } }) => onInput(value)}
    >
      {() => (
        <TextField
          autoComplete="off"
          className={classes.input}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon />
              </InputAdornment>
            ),
          }}
          {...props}
        />
      )}
    </InputMask>
  ) : (
    <TextField
      autoComplete="off"
      className={classes.input}
      value={inputValue}
      onChange={({ target: { value } }) => onInput(value)}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Icon />
          </InputAdornment>
        ),
      }}
      {...props}
    />
  );
};

Input.propTypes = {
  icon: PropTypes.func.isRequired,
  onInput: PropTypes.func.isRequired,
  mask: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Input;
