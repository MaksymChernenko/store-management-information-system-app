import { StepConnector } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const ColorlibStepperConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg, #767b85 0%, #373a40 50%, #2b2d30 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg, #767b85 0%, #373a40 50%, #2b2d30 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

export default ColorlibStepperConnector;
