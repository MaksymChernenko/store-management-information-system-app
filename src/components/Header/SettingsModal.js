import PropTypes from 'prop-types';

import { Tab, Tabs } from '@material-ui/core';
import { Add, Edit, Info } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as sessionActions from '../../actions/session';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { a11yTabProps, useVerticalTabStyles, isCreator } from '../../utils';

import { Modal } from '..';
import TabPanel from '../TabPanel';
import AddUsers from './AddUsers';
import EditUsers from './EditUsers';
import StoreInfo from './StoreInfo';

const SettingsModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();
  const classes = useVerticalTabStyles();

  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [value, setValue] = useState(0);

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    dispatch(sessionActions.fetchUsers());
  }, []);

  return (
    <Modal
      open={open}
      handleClose={() => {
        handleClose();
        setValue(0);
      }}
    >
      <SettingsModal.Wrapper>
        <div className={classes.root}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={value}
            onChange={handleChange}
            aria-label="Vertical tabs"
            className={classes.tabs}
          >
            <Tab
              className={classes.tab}
              icon={<Add />}
              label="Додати"
              {...a11yTabProps(0)}
            />
            <Tab
              className={classes.tab}
              icon={<Edit />}
              label="Редагувати"
              {...a11yTabProps(1)}
            />
            {isCreator(store, user.id) && (
              <Tab
                className={classes.tab}
                icon={<Info />}
                label="Інформація"
                {...a11yTabProps(2)}
              />
            )}
          </Tabs>
          <TabPanel value={value} index={0}>
            <AddUsers />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <EditUsers />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <StoreInfo handleClose={handleClose} />
          </TabPanel>
        </div>
      </SettingsModal.Wrapper>
    </Modal>
  );
};

SettingsModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

SettingsModal.Wrapper = styled.div`
  width: 740px;
  margin-top: -6px;
`;

export default SettingsModal;
