import { Button } from '@material-ui/core';

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as sessionActions from '../../actions/session';
import * as storeActions from '../../actions/store';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { Select } from '..';

const AddUsers = () => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);
  const currentUser = useSelector(sessionSelectors.getUser);
  const users = useSelector(sessionSelectors.getUsers);

  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const [user, setUser] = useState(null);

  const handleAddUserToStore = async () => {
    try {
      await dispatch(
        sessionActions.addStoreToUser(
          {
            id: user.value,
            payload: {
              storeId: store.id,
              withoutUpdate: true,
            },
          },
          `${currentUser.name} додав користувача ${user.label} до магазину`,
        ),
      );
      await dispatch(
        storeActions.fetchStore({
          payload: {
            id: store.id,
          },
        }),
      );
      dispatch(sessionActions.fetchUsers());
      setError('');
      setSuccess('Користувач доданий до магазину.');
    } catch (err) {
      setSuccess('');
      setError('Цей користувач вже є у цьому магазині.');
    }
  };

  return (
    <AddUsers.Wrapper>
      <AddUsers.SelectWrapper>
        <Select
          autoFocus
          placeholder="Вибрати користувача"
          value={user}
          width={240}
          onChange={val => setUser(val)}
          // eslint-disable-next-line no-underscore-dangle
          options={users.map(v => ({ value: v._id, label: v.name }))}
        />
      </AddUsers.SelectWrapper>
      <Button
        disabled={!user}
        variant="outlined"
        onClick={handleAddUserToStore}
      >
        Додати до магазину
      </Button>
      {error && <AddUsers.StatusBox error>{error}</AddUsers.StatusBox>}
      {success && <AddUsers.StatusBox>{success}</AddUsers.StatusBox>}
    </AddUsers.Wrapper>
  );
};

AddUsers.Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

AddUsers.SelectWrapper = styled.div`
  width: 240px;
`;

AddUsers.StatusBox = styled.p`
  position: absolute;
  width: 100%;
  padding: 5px 10px;
  margin: 0;
  box-sizing: border-box;
  top: 360px;
  left: 0;
  background-color: ${({ error }) => (error ? '#fbe5e1' : '#e9fbe1')};
  color: ${({ error }) => (error ? '#a62d19' : '#1ea619')};
`;

export default AddUsers;
