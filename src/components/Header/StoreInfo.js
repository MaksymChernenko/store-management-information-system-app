import PropTypes from 'prop-types';

import { Button, TextField, Tooltip } from '@material-ui/core';
import { Store } from '@material-ui/icons';

import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as storeActions from '../../actions/store';
import * as storeSelectors from '../../selectors/store';

import { Input, confirm } from '..';

const StoreInfo = ({ handleClose }) => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);

  const [storeName, setStoreName] = useState('');
  const [deleteConfirmationWord, setDeleteConfirmationWord] = useState('');

  useEffect(() => {
    setStoreName(store.name);
  }, [store]);

  const handleUpdateStore = useCallback(
    async e => {
      e.preventDefault();
      await dispatch(
        storeActions.updateStore({
          payload: { name: storeName },
          id: store.id,
        }),
      );
      dispatch(
        storeActions.fetchStore({
          payload: {
            id: store.id,
          },
        }),
      );
    },
    [storeName],
  );

  const handleDeleteStore = useCallback(async e => {
    e.preventDefault();
    if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
      await dispatch(
        storeActions.deleteStore({
          id: store.id,
        }),
      );
      handleClose();
      try {
        await dispatch(
          storeActions.fetchStore({
            payload: {
              id: store.id,
            },
          }),
        );
      } catch (err) {
        dispatch(storeActions.clearStore());
      }
    }
  }, []);

  return (
    store && (
      <>
        <StoreInfo.Title>Редагувати магазин</StoreInfo.Title>
        <StoreInfo.EditForm onSubmit={handleUpdateStore}>
          <Input
            autoFocus
            label="Ім'я магазину"
            value={storeName}
            onInput={value => setStoreName(value)}
            style={{ marginRight: 20 }}
            icon={Store}
          />
          <Button variant="outlined" type="submit">
            Оновити
          </Button>
        </StoreInfo.EditForm>
        <StoreInfo.Title>Видалити магазин</StoreInfo.Title>
        <StoreInfo.EditForm onSubmit={handleDeleteStore}>
          <Tooltip
            title="Введіть ім'я магазину для підтвердження"
            placement="bottom"
            leaveDelay={500}
          >
            <TextField
              value={deleteConfirmationWord}
              onChange={({ target: { value } }) =>
                setDeleteConfirmationWord(value)
              }
              style={{ marginRight: 20 }}
            />
          </Tooltip>
          <Button
            disabled={deleteConfirmationWord !== store.name}
            variant="outlined"
            color="secondary"
            type="submit"
          >
            Видалити
          </Button>
        </StoreInfo.EditForm>
      </>
    )
  );
};

StoreInfo.propTypes = {
  handleClose: PropTypes.func.isRequired,
};

StoreInfo.Title = styled.p`
  margin: 0 0 20px;
`;

StoreInfo.EditForm = styled.form`
  position: relative;
  display: flex;
  align-items: flex-end;
  margin-bottom: 40px;
`;

StoreInfo.DeleteForm = styled.form`
  position: relative;
  display: flex;
  align-items: flex-end;
`;

export default StoreInfo;
