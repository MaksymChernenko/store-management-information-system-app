/* eslint-disable no-underscore-dangle */
import { Button, IconButton, Tooltip } from '@material-ui/core';
import { CancelOutlined } from '@material-ui/icons';

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as sessionActions from '../../actions/session';
import * as storeActions from '../../actions/store';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { confirm } from '..';

import { isAdmin, isCreator } from '../../utils';

const EditUsers = () => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);
  const currentUser = useSelector(sessionSelectors.getUser);
  const users = useSelector(sessionSelectors.getUsers).filter(user =>
    user.stores.includes(store.id),
  );

  const handleDeleteUserFromStore = async (user, shouldDelete) => {
    if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
      await dispatch(
        sessionActions.deleteUserFromStore(
          {
            id: user._id,
            storeId: store.id,
          },
          `${currentUser.name} видалив користувача ${user.name} з магазину`,
        ),
      );
      if (shouldDelete) {
        await dispatch(
          storeActions.deleteAdminRights({
            id: store.id,
            userId: user._id,
          }),
        );
      }
      await dispatch(sessionActions.fetchUsers());
      await dispatch(
        storeActions.fetchStore({
          payload: {
            id: store.id,
          },
        }),
      );
    }
  };

  const handleSetAdminRights = async (user, shouldDelete) => {
    if (shouldDelete) {
      await dispatch(
        storeActions.deleteAdminRights(
          {
            id: store.id,
            userId: user._id,
          },
          `${currentUser.name} видалив права адміністратора у ${user.name}`,
        ),
      );
    } else {
      await dispatch(
        storeActions.setAdminRights(
          {
            id: store.id,
            payload: {
              userId: user._id,
            },
          },
          `${currentUser.name} надав права адміністратора користувачу ${user.name}`,
        ),
      );
    }
    await dispatch(
      storeActions.fetchStore({
        payload: {
          id: store.id,
        },
      }),
    );
  };

  return (
    <div>
      <EditUsers.Title>Користувачі</EditUsers.Title>
      <ul style={{ margin: 0, padding: 0, listStyle: 'none' }}>
        {users.map(user => (
          <EditUsers.Item key={user._id} creator={isCreator(store, user._id)}>
            {isAdmin(store, user._id) && (
              <EditUsers.Sign type="admin">A</EditUsers.Sign>
            )}
            {isCreator(store, user._id) && (
              <EditUsers.Sign type="creator">C</EditUsers.Sign>
            )}
            <div
              style={{
                overflowX: isCreator(store, user._id) ? 'none' : 'auto',
              }}
            >
              <EditUsers.ItemText withSeparator>{user.name}</EditUsers.ItemText>
              <EditUsers.ItemText>{user.email}</EditUsers.ItemText>
            </div>
            <EditUsers.Actions>
              {!isCreator(store, user._id) && (
                <Tooltip
                  title="Видалити користувача з магазину"
                  aria-label="delete"
                >
                  <IconButton
                    disabled={
                      isCreator(store, currentUser.id)
                        ? false
                        : !!isAdmin(store, user._id)
                    }
                    color="secondary"
                    onClick={() =>
                      handleDeleteUserFromStore(
                        user,
                        isCreator(store, currentUser.id) &&
                          isAdmin(store, user._id),
                      )
                    }
                  >
                    <CancelOutlined />
                  </IconButton>
                </Tooltip>
              )}
              {!isCreator(store, user._id) && (
                <Button
                  disabled={
                    isCreator(store, currentUser.id)
                      ? false
                      : !!isAdmin(store, user._id)
                  }
                  style={{
                    color:
                      isCreator(store, currentUser.id) &&
                      isAdmin(store, user._id)
                        ? 'red'
                        : isAdmin(store, user._id)
                        ? 'gray'
                        : '#00b029',
                  }}
                  onClick={() =>
                    handleSetAdminRights(
                      user,
                      isCreator(store, currentUser.id) &&
                        isAdmin(store, user._id),
                    )
                  }
                >
                  {isCreator(store, currentUser.id) && isAdmin(store, user._id)
                    ? 'Видалити права адміністратора'
                    : 'Надати права адміністратора'}
                </Button>
              )}
            </EditUsers.Actions>
          </EditUsers.Item>
        ))}
      </ul>
    </div>
  );
};

EditUsers.Title = styled.p`
  margin: 0 0 20px;
`;

EditUsers.Item = styled.li`
  position: relative;
  width: 522px;
  padding: ${({ creator }) =>
    creator ? '12px 26px 12px 26px' : '12px 328px 12px 26px'};
  border: 1px ridge #c8d0e3;
  margin-bottom: 10px;
  box-sizing: border-box;
`;

EditUsers.ItemText = styled.span`
  margin-right: 10px;
  padding-right: 10px;
  border-right: ${({ withSeparator }) => withSeparator && '1px solid #c9ced6'};
  line-height: 1;
`;

EditUsers.Sign = styled.span`
  font-size: 12px;
  color: ${({ type }) => (type === 'creator' ? 'green' : 'red')};
  position: absolute;
  left: 4px;
  top: 4px;
`;

EditUsers.Actions = styled.div`
  width: 324px;
  display: flex;
  justify-content: space-between;
  position: absolute;
  top: 0;
  right: 6px;
`;

export default EditUsers;
