import {
  AppBar,
  Avatar,
  Button,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import { AccountBalance, Add } from '@material-ui/icons';

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import { signOut } from '../../actions/session';
import { getUser } from '../../selectors/session';
import { getStore } from '../../selectors/store';

import { routePaths } from '../../routes';
import { isAdmin, isCreator, StyledBadge } from '../../utils';

import SettingsModal from './SettingsModal';

const useStyles = makeStyles(() => ({
  appBar: {
    backgroundColor: '#373a40',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    marginRight: 20,
  },
  subtitle: {
    marginTop: 4,
  },
}));

const Header = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const user = useSelector(getUser);
  const store = useSelector(getStore);

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [openSettings, setOpenSettings] = useState(false);

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenSettings = () => {
    setOpenSettings(true);
  };

  const handleCloseSettings = () => {
    setOpenSettings(false);
  };

  const handleGoToSelectPage = () => {
    history.push(routePaths.selectStore);
  };

  const handleSignOut = async () => {
    try {
      await dispatch(signOut());
      history.push(routePaths.signIn);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  };

  return (
    <AppBar className={classes.appBar} position="static">
      <Toolbar className={classes.toolbar}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Typography variant="h6" className={classes.title}>
            {location.pathname.startsWith('/app/store/') && store
              ? store.name
              : 'Інформаційно-Аналітична Система'}
          </Typography>
          {store && location.pathname.startsWith('/app/store/') && (
            <Typography variant="subtitle2" className={classes.subtitle}>
              {`Права користувача: `}
              {isAdmin(store, user.id) ? (
                <span style={{ color: '#ff5959' }}>Адміністратор</span>
              ) : isCreator(store, user.id) ? (
                <span style={{ color: '#6cff59' }}>Власник</span>
              ) : (
                <span style={{ color: '#59c5ff' }}>Користувач</span>
              )}
            </Typography>
          )}
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {location.pathname.startsWith('/app/store/') && (
            <>
              <Button
                style={{
                  padding: '2px 6px',
                  margin: '3px 40px 0 0',
                  color: '#fff',
                }}
                onClick={handleGoToSelectPage}
              >
                Повернутися до вибору магазну
              </Button>
              {store && (isAdmin(store, user.id) || isCreator(store, user.id)) && (
                <Button
                  variant="outlined"
                  style={{
                    padding: '2px 6px',
                    margin: '3px 40px 0 0',
                    color: '#fff',
                  }}
                  onClick={handleOpenSettings}
                  endIcon={<Add />}
                >
                  Налаштування
                </Button>
              )}
            </>
          )}
          <Typography
            style={{ fontSize: 18, marginRight: 10 }}
            variant="caption"
          >
            {user.name}
          </Typography>
          <StyledBadge
            style={{ marginRight: 20, cursor: 'pointer' }}
            overlap="circle"
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            variant="dot"
            onClick={handleMenu}
          >
            <Avatar>{user.name.slice(0, 1)}</Avatar>
          </StyledBadge>
          <AccountBalance style={{ width: 32, height: 32 }} />
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={handleSignOut}>Вийти з системи</MenuItem>
          </Menu>
        </div>
        <SettingsModal open={openSettings} handleClose={handleCloseSettings} />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
