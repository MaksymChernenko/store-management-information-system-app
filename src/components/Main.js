import PropTypes from 'prop-types';

import React from 'react';
import styled from 'styled-components';

import { Header } from '.';

const Main = ({ children }) => (
  <div>
    <Header />
    <Main.Container id="main">{children}</Main.Container>
  </div>
);

Main.propTypes = {
  children: PropTypes.node.isRequired,
};

Main.Container = styled.div`
  height: calc(100vh - 64px);
  overflow-y: auto;
`;

export default Main;
