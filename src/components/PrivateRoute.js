import PropTypes from 'prop-types';

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getAuthStatus } from '../selectors/session';

const PrivateRoute = ({ render: Component, ...rest }) => {
  const isAuthenticated = useSelector(getAuthStatus);

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              // eslint-disable-next-line react/prop-types
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

PrivateRoute.propTypes = {
  render: PropTypes.func.isRequired,
};

export default PrivateRoute;
