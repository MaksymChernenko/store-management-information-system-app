import PropTypes from 'prop-types';

import { CircularProgress } from '@material-ui/core';

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import { fetchStore } from '../actions/store';
import { getUser } from '../selectors/session';
import { getStoreState } from '../selectors/store';

import { routePaths } from '../routes';

import { FETCHING_STATE } from '../services/reduxHelpers';

const Store = ({ children }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const storeState = useSelector(getStoreState);
  const user = useSelector(getUser);

  useEffect(() => {
    if (!user.stores.includes(id)) {
      history.push(routePaths.permissionDenied);
    } else {
      dispatch(fetchStore({ payload: { id } }));
    }
  }, []);

  return storeState === FETCHING_STATE.FETCHING ? (
    <Store.Loader>
      <CircularProgress />
    </Store.Loader>
  ) : storeState === FETCHING_STATE.ERROR ? (
    <Redirect to={routePaths.selectStore} />
  ) : (
    <>{children}</>
  );
};

Store.propTypes = {
  children: PropTypes.node.isRequired,
};

Store.Loader = styled.div`
  height: calc(100vh - 64px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Store;
