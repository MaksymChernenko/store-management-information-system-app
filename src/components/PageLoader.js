import { CircularProgress } from '@material-ui/core';

import React from 'react';
import styled from 'styled-components';

const PageLoader = () => (
  <PageLoader.Wrapper>
    <CircularProgress />
  </PageLoader.Wrapper>
);

PageLoader.Wrapper = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default PageLoader;
