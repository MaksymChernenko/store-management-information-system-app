import PropTypes from 'prop-types';

import IconButton from '@material-ui/core/IconButton';

import * as React from 'react';
import styled from 'styled-components';

const IconCellRenderer = ({ data, items, value }) => (
  <IconCellRenderer.Wrapper>
    {items.map(({ icon: Icon, onClick }, idx) => (
      <IconButton
        // eslint-disable-next-line react/no-array-index-key
        key={idx}
        style={{ padding: 0, margin: '0 10px', color: '#fff' }}
        onClick={() => onClick(value, data)}
      >
        <Icon />
      </IconButton>
    ))}
  </IconCellRenderer.Wrapper>
);

IconCellRenderer.propTypes = {
  data: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  value: PropTypes.string.isRequired,
};

IconCellRenderer.Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default IconCellRenderer;
