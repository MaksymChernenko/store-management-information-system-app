import styled from 'styled-components';

export const FormGroupTitle = styled.h5`
  background-color: #fff;
  position: absolute;
  top: -22px;
  margin: 0;
  padding: 0 5px;
  font-size: 16px;
  font-weight: 500;
  text-transform: uppercase;
`;

export const FormGroupTitleContainer = styled.div`
  position: relative;
`;

export const InfoBlock = styled.div`
  width: ${({ width }) => width || '33%'};
  border: 1px solid #ccc;
  padding: 10px;
`;
