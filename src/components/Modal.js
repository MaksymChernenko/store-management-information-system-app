import PropTypes from 'prop-types';

import { Backdrop, Fade, Modal } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import React from 'react';
import styled from 'styled-components';

const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const StandardModal = ({ children, handleClose, open }) => {
  const classes = useStyles();
  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <StandardModal.ProcessWrapper>{children}</StandardModal.ProcessWrapper>
      </Fade>
    </Modal>
  );
};

StandardModal.propTypes = {
  children: PropTypes.node.isRequired,
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

StandardModal.ProcessWrapper = styled.div`
  background-color: #fff;
  border-radius: 16px;
  position: relative;
  box-sizing: border-box;
  padding: 24px 42px 16px;

  &:focus {
    outline: none;
  }
`;

export default StandardModal;
