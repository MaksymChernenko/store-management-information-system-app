import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import { CloudUpload } from '@material-ui/icons';

import React from 'react';

const excelMIMETypes = [
  'application/vnd.ms-excel',
  'application/msexcel',
  'application/x-msexcel',
  'application/x-ms-excel',
  'application/x-excel',
  'application/x-dos_ms_excel',
  'application/xls',
  'application/x-xls',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
];

const UploadExcelFileInput = ({ style, ...props }) => (
  <>
    <input
      accept={excelMIMETypes.join(',')}
      style={{ display: 'none' }}
      id="import-data"
      type="file"
      {...props}
    />
    <label htmlFor="import-data">
      <Button component="span" style={style} startIcon={<CloudUpload />}>
        Імпорт даних
      </Button>
    </label>
  </>
);

UploadExcelFileInput.propTypes = {
  style: PropTypes.object.isRequired,
};

export default UploadExcelFileInput;
