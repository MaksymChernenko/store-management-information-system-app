import PropTypes from 'prop-types';

import { CircularProgress } from '@material-ui/core';

import React from 'react';
import styled from 'styled-components';

const LoaderOverlay = ({ children, isFetching = false }) => (
  <LoaderOverlay.Wrapper>
    {children}
    {isFetching && (
      <LoaderOverlay.Loader>
        <CircularProgress style={{ width: 60, height: 60, color: '#f2f2f2' }} />
      </LoaderOverlay.Loader>
    )}
  </LoaderOverlay.Wrapper>
);

LoaderOverlay.propTypes = {
  children: PropTypes.node.isRequired,
  isFetching: PropTypes.bool.isRequired,
};

LoaderOverlay.Wrapper = styled.div`
  position: relative;
`;

LoaderOverlay.Loader = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

export default LoaderOverlay;
