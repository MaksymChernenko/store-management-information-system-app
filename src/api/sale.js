import axios from 'axios';

export const fetchSales = async ({ storeId }) => {
  const url = `/sale/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createSale = async ({ payload }) => {
  const url = '/sale';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateSale = async ({ payload, id }) => {
  const url = `/sale/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteSale = async ({ id }) => {
  const url = `/sale/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
