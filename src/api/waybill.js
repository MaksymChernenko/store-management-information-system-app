import axios from 'axios';

export const fetchWaybills = async ({ storeId }) => {
  const url = `/waybill/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createWaybill = async ({ payload }) => {
  const url = '/waybill';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateWaybill = async ({ payload, id }) => {
  const url = `/waybill/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteWaybill = async ({ id }) => {
  const url = `/waybill/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
