import axios from 'axios';

export const trackTheWorkOfEmployees = async ({ employeeId }) => {
  const url = `/analysis/employee/${employeeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const trackCollaborationWithSuppliers = async ({ supplierId }) => {
  const url = `/analysis/supplier/${supplierId}`;

  const { data } = await axios.get(url);

  return data;
};

export const checkProductsRate = async () => {
  const url = '/analysis/product/rate';

  const { data } = await axios.get(url);

  return data;
};
