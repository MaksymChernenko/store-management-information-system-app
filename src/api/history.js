import axios from 'axios';

export const fetchNotes = async ({ storeId }) => {
  const url = `/history/notes/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const addNote = async ({ payload }) => {
  const url = '/history/notes';

  const { data } = await axios.post(url, payload);

  return data;
};
