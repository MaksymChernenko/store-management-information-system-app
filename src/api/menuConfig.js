import axios from 'axios';

export const fetchMenuConfig = async ({ storeId, userId, configType }) => {
  const url = `/menu-config/${storeId}/${userId}/${configType}`;

  const { data } = await axios.get(url);

  return data;
};

export const addMenuConfig = async ({ payload }) => {
  const url = '/menu-config';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateMenuConfig = async ({ id, payload }) => {
  const url = `/menu-config/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};
