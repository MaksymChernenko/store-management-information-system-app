import axios from 'axios';

export const fetchEmployees = async ({ storeId }) => {
  const url = `/employee/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createEmployee = async ({ payload }) => {
  const url = '/employee';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateEmployee = async ({ payload, id }) => {
  const url = `/employee/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteEmployee = async ({ id }) => {
  const url = `/employee/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
