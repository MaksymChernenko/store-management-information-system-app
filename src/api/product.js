import axios from 'axios';

export const fetchProducts = async ({ storeId }) => {
  const url = `/product/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createProduct = async ({ payload }) => {
  const url = '/product';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateProduct = async ({ payload, id }) => {
  const url = `/product/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteProduct = async ({ id }) => {
  const url = `/product/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
