import axios from 'axios';

export const fetchPositions = async ({ storeId }) => {
  const url = `/position/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createPosition = async ({ payload }) => {
  const url = '/position';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updatePosition = async ({ payload, id }) => {
  const url = `/position/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deletePosition = async ({ id }) => {
  const url = `/position/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
