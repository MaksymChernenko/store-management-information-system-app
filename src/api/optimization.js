import axios from 'axios';

export const calculateProductProfit = async ({
  productId,
  changePriceTo,
  fixedCosts,
}) => {
  const url = `/optimization/product-profit/${productId}/${changePriceTo}/${fixedCosts}`;

  const { data } = await axios.get(url);

  return data;
};

export const buildProfitRetentionCurve = async ({ productId }) => {
  const url = `/optimization/profit-retention/${productId}`;

  const { data } = await axios.get(url);

  return data;
};
