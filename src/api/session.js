import axios from 'axios';

export const signUp = async ({ payload }) => {
  const url = '/auth/signup';

  const { data } = await axios.post(url, payload);

  return data;
};

export const signIn = async ({ payload }) => {
  const url = '/auth/signin';

  const { data } = await axios.post(url, payload);

  return data;
};

export const signOut = async () => {
  const url = '/auth/signout';

  const { data } = await axios.post(url);

  return data;
};

export const refreshUser = async () => {
  const url = '/auth/current';

  const { data } = await axios.get(url);

  return data;
};

export const fetchUsers = async () => {
  const url = '/auth/user';

  const { data } = await axios.get(url);

  return data;
};

export const addStoreToUser = async ({ payload, id }) => {
  const url = `/auth/user/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteUserFromStore = async ({ id, storeId }) => {
  const url = `/auth/user/${id}/${storeId}`;

  const { data } = await axios.delete(url);

  return data;
};
