import axios from 'axios';

export const fetchSuppliers = async ({ storeId }) => {
  const url = `/supplier/${storeId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createSupplier = async ({ payload }) => {
  const url = '/supplier';

  const { data } = await axios.post(url, payload);

  return data;
};

export const updateSupplier = async ({ payload, id }) => {
  const url = `/supplier/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteSupplier = async ({ id }) => {
  const url = `/supplier/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
