import axios from 'axios';

export const fetchStores = async () => {
  const url = '/store';

  const { data } = await axios.get(url);

  return data;
};

export const fetchStore = async ({ payload }) => {
  const url = `/store/${payload.id}`;

  const { data } = await axios.get(url);

  return data;
};

export const createStore = async ({ payload }) => {
  const url = '/store';

  const { data } = await axios.post(url, payload);

  return data;
};

export const setAdminRights = async ({ payload, id }) => {
  const url = `/store/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteAdminRights = async ({ id, userId }) => {
  const url = `/store/${id}/${userId}`;

  const { data } = await axios.delete(url);

  return data;
};

export const updateStore = async ({ payload, id }) => {
  const url = `/store/info/${id}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteStore = async ({ id }) => {
  const url = `/store/${id}`;

  const { data } = await axios.delete(url);

  return data;
};
