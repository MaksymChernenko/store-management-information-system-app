import * as R from 'ramda';

import { ROOT } from '../reducers/store';

export const getStores = state => R.path([ROOT, 'stores', 'data'], state);

export const getStore = state => R.path([ROOT, 'store', 'data'], state);

export const getStoreState = state => R.path([ROOT, 'store', 'state'], state);
