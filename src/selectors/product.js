import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as PRODUCT_ROOT } from '../reducers/entries/product';

export const getProducts = state =>
  R.path([[ENTRIES_ROOT], [PRODUCT_ROOT], 'products', 'data'], state);

export const getProductsState = state =>
  R.path([[ENTRIES_ROOT], [PRODUCT_ROOT], 'products', 'state'], state);
