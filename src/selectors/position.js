import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as POSITION_ROOT } from '../reducers/entries/position';

export const getPositions = state =>
  R.path([[ENTRIES_ROOT], [POSITION_ROOT], 'positions', 'data'], state);

export const getPositionsState = state =>
  R.path([[ENTRIES_ROOT], [POSITION_ROOT], 'positions', 'state'], state);
