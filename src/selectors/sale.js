import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as SALE_ROOT } from '../reducers/entries/sale';

export const getSales = state =>
  R.path([[ENTRIES_ROOT], [SALE_ROOT], 'sales', 'data'], state);

export const getSalesState = state =>
  R.path([[ENTRIES_ROOT], [SALE_ROOT], 'sales', 'state'], state);
