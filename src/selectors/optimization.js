/* eslint-disable import/prefer-default-export */
import * as R from 'ramda';

import { ROOT } from '../reducers/optimization';

export const getProductProfit = state => R.path([ROOT, 'productProfit'], state);

export const getProfitRetentionCurveData = state =>
  R.path([ROOT, 'profitRetentionCurveData'], state);
