import * as R from 'ramda';

import { ROOT } from '../reducers/session';

export const getAuthStatus = state => R.path([ROOT, 'isAuthenticated'], state);

export const getRefreshStatus = state => R.path([ROOT, 'isRefreshed'], state);

export const getUser = state => R.path([ROOT, 'user'], state);

export const getToken = state => R.path([ROOT, 'token'], state);

export const getUsers = state => R.path([ROOT, 'users'], state);
