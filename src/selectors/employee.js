import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as EMPLOYEE_ROOT } from '../reducers/entries/employee';

export const getEmployees = state =>
  R.path([[ENTRIES_ROOT], [EMPLOYEE_ROOT], 'employees', 'data'], state);

export const getEmployeesState = state =>
  R.path([[ENTRIES_ROOT], [EMPLOYEE_ROOT], 'employees', 'state'], state);
