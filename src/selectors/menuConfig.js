import * as R from 'ramda';

import { ROOT } from '../reducers/menuConfig';

export const getStoreMenuConfig = state =>
  R.path([[ROOT], 'storeMenu', 'data'], state);

export const getStoreMenuConfigState = state =>
  R.path([[ROOT], 'storeMenu', 'state'], state);

export const getAnalysisMenuConfig = state =>
  R.path([[ROOT], 'analysisMenu', 'data'], state);

export const getAnalysisMenuConfigState = state =>
  R.path([[ROOT], 'analysisMenu', 'state'], state);
