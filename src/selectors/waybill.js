import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as WAYBILL_ROOT } from '../reducers/entries/waybill';

export const getWaybills = state =>
  R.path([[ENTRIES_ROOT], [WAYBILL_ROOT], 'waybills', 'data'], state);

export const getWaybillsState = state =>
  R.path([[ENTRIES_ROOT], [WAYBILL_ROOT], 'waybills', 'state'], state);
