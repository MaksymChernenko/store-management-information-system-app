import * as R from 'ramda';

import { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import { ROOT as SUPPLIER_ROOT } from '../reducers/entries/supplier';

export const getSuppliers = state =>
  R.path([[ENTRIES_ROOT], [SUPPLIER_ROOT], 'suppliers', 'data'], state);

export const getSuppliersState = state =>
  R.path([[ENTRIES_ROOT], [SUPPLIER_ROOT], 'suppliers', 'state'], state);
