import * as R from 'ramda';
import { createSelector } from 'reselect';

import { ROOT } from '../reducers/history';

export const getNotes = state => R.path([ROOT, 'notes', 'data'], state);

export const getNotesState = state => R.path([ROOT, 'notes', 'state'], state);

export const getFilters = state => R.path([ROOT, 'notes', 'filters'], state);

const userFilter = filters => note => {
  const { user } = filters;
  return user ? note.user === user : true;
};

export const getFilteredNotes = createSelector(
  [getNotes, getFilters],
  (notes, filters) => notes.filter(userFilter(filters)),
);
