import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

import appHistoryMiddleware from './middlewares/appHistory';
import authHeaderMiddleware from './middlewares/authHeader';
import rootReducer from './rootReducer';

const middlewares = [thunk, authHeaderMiddleware, appHistoryMiddleware];

const enhancer = composeWithDevTools(applyMiddleware(...middlewares));

const store = createStore(rootReducer, enhancer);

const persistor = persistStore(store);

export { persistor, store };
