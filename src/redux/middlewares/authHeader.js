import { clearAuthHeader, setAuthHeader } from '../../services/axiosDefaults';
import { SUCCESS, ERROR, START } from '../../services/reduxHelpers';

import * as actions from '../../actions/session';

const authHeaderMiddleware = ({ getState }) => next => action => {
  if (
    action.type === actions.REFRESH_USER[START] ||
    action.type === actions.REFRESH_USER[SUCCESS]
  ) {
    const state = getState();
    const { token } = state.session;

    if (!token) return;

    setAuthHeader(token);
  }
  if (
    action.type === actions.SIGN_UP[SUCCESS] ||
    action.type === actions.SIGN_IN[SUCCESS]
  ) {
    const { token } = action.data;
    setAuthHeader(token);
  }
  if (
    action.type === actions.SIGN_OUT[SUCCESS] ||
    action.type === actions.REFRESH_USER[ERROR]
  ) {
    clearAuthHeader();
  }
  next(action);
};

export default authHeaderMiddleware;
