import * as R from 'ramda';

import * as actions from '../../actions/history';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

const appHistoryMiddleware = ({ dispatch, getState }) => next => action => {
  const { note } = action;
  if (note) {
    const state = getState();
    const user = sessionSelectors.getUser(state);
    const storeId = storeSelectors.getStore(state).id;
    const name = R.propOr('', 'name', user);

    const payload = {
      user: name,
      action: note,
      storeId,
    };

    dispatch(actions.addNote({ payload }));
  }
  next(action);
};

export default appHistoryMiddleware;
