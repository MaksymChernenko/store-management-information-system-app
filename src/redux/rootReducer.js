import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import sessionReducer, { ROOT as SESSION_ROOT } from '../reducers/session';
import historyReducer, { ROOT as HISTORY_ROOT } from '../reducers/history';
import storeReducer, { ROOT as STORE_ROOT } from '../reducers/store';
import entriesReducer, { ROOT as ENTRIES_ROOT } from '../reducers/entries';
import menuConfigReducer, {
  ROOT as MENU_CONFIG_ROOT,
} from '../reducers/menuConfig';
import optimizationReducer, {
  ROOT as OPTIMIZATION_ROOT,
} from '../reducers/optimization';

const tokenPersistConfig = {
  key: 'token',
  storage,
  whitelist: ['token'],
};

const rootReducer = combineReducers({
  [SESSION_ROOT]: persistReducer(tokenPersistConfig, sessionReducer),
  [HISTORY_ROOT]: historyReducer,
  [STORE_ROOT]: storeReducer,
  [ENTRIES_ROOT]: entriesReducer,
  [MENU_CONFIG_ROOT]: menuConfigReducer,
  [OPTIMIZATION_ROOT]: optimizationReducer,
});

export default rootReducer;
