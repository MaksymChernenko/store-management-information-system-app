import { Badge, Chip } from '@material-ui/core';
import { emphasize, withStyles, makeStyles } from '@material-ui/core/styles';
import {
  Apartment,
  Assessment,
  ChromeReaderMode,
  Settings,
} from '@material-ui/icons';

import moment from 'moment';

export const getOnClickStoreCardHandler = (history, id) => ({
  'Налаштування облікових записів': () =>
    history.push(`/app/store/${id}/entries-config`),
  'Життєвий цикл системи': () =>
    history.push(`/app/store/${id}/store-lifecycle`),
  'Аналіз роботи магазину': () =>
    history.push(`/app/store/${id}/store-analysis`),
  'Оптимізація виручки': () =>
    history.push(`/app/store/${id}/revenue-optimization`),
});

export const getStoreCardIcon = {
  'Налаштування облікових записів': Settings,
  'Життєвий цикл системи': Apartment,
  'Аналіз роботи магазину': ChromeReaderMode,
  'Оптимізація виручки': Assessment,
};

export const storeMenuCards = [
  {
    title: 'Налаштування облікових записів',
  },
  {
    title: 'Життєвий цикл системи',
  },
  {
    title: 'Аналіз роботи магазину',
  },
  {
    title: 'Оптимізація виручки',
  },
];

export const StyledBreadcrumb = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.grey[100],
    height: theme.spacing(3),
    color: theme.palette.grey[800],
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: theme.palette.grey[300],
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
  },
}))(Chip);

export const StyledBadge = withStyles(theme => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

export const a11yTabProps = index => ({
  id: `vertical-tab-${index}`,
  'aria-controls': `vertical-tabpanel-${index}`,
});

export const getElapsedTime = createdAt =>
  moment.utc(moment(createdAt)).fromNow();

export const useVerticalTabStyles = makeStyles(theme => ({
  root: {
    boxShadow: '0px 0px 1px',
    alignSelf: 'flex-start',
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 440,
    width: '100%',
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    '& .MuiTab-textColorInherit.Mui-selected': {
      backgroundColor: '#edf4ff',
      filter: 'grayscale(0.6)',
    },
    '& .PrivateTabIndicator-colorSecondary-203': {
      backgroundColor: '#00acf5',
    },
  },
  tab: {
    width: 220,
    '& .MuiTab-wrapper': {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    '& .MuiSvgIcon-root': {
      margin: '4px 10px 0 0',
    },
  },
}));

export const isAdmin = (store, userId) =>
  store && userId && store.admins.includes(userId);

export const isCreator = (store, userId) =>
  store && userId && store.creator === userId;

export const paymentTypes = ['cash', 'card'];

export const dateFormatter = ({ value }) => (value ? value.split('T')[0] : '');

export const getProductProfitData = productProfit => {
  if (productProfit) {
    return [
      {
        title: `Об'єм продажу (шт)`,
        source: productProfit.source.volumeOfSales,
        estimated: productProfit.estimated.volumeOfSales,
      },
      {
        title: 'Виручка (грн)',
        source: productProfit.source.revenue,
        estimated: productProfit.estimated.revenue,
      },
      {
        title: 'Витрати змінні (грн)',
        source: productProfit.source.variableCosts,
        estimated: productProfit.estimated.variableCosts,
      },
      {
        title: 'Постійні витрати (грн)',
        source: productProfit.source.fixedCosts,
        estimated: productProfit.estimated.fixedCosts,
      },
      {
        title: 'Прибуток',
        source: productProfit.source.profit,
        estimated: productProfit.estimated.profit,
      },
    ];
  }
  return [];
};

export const getTargetPriceChangeNaturalValueCount = productProfit =>
  productProfit.targetPriceChange > 0
    ? productProfit.targetPriceChange
    : productProfit.targetPriceChange * -1;

export const getNecessaryVolumeNaturalValuePct = productProfit =>
  productProfit.necessaryVolume > 0
    ? productProfit.necessaryVolume
    : productProfit.necessaryVolume * -1;

export const getNecessaryVolumeNaturalValueCount = productProfit => {
  const result =
    productProfit.source.volumeOfSales - productProfit.estimated.volumeOfSales;
  return result > 0 ? result : result * -1;
};
