/* eslint-disable no-underscore-dangle */
import { Button, LinearProgress } from '@material-ui/core';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as analysisActions from '../../actions/analysis';
import * as employeeActions from '../../actions/employee';
import * as employeeSelectors from '../../selectors/employee';
import * as storeSelectors from '../../selectors/store';

import { Select } from '../../components';

const EmployeeItem = () => {
  const dispatch = useDispatch();

  const employees = useSelector(employeeSelectors.getEmployees);
  const store = useSelector(storeSelectors.getStore);

  const [employee, setEmployee] = useState('');
  const [productSalesCount, setProductSalesCount] = useState('');
  const [employeeEarning, setEmployeeEarning] = useState('');

  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (employees.length === 0) {
      dispatch(employeeActions.fetchEmployees({ storeId: store.id }));
    }
  }, []);

  const handleCheck = async e => {
    e.preventDefault();
    setIsError(false);
    try {
      setIsLoading(true);
      const { earning, salesCount } = await dispatch(
        analysisActions.trackTheWorkOfEmployees({ employeeId: employee.value }),
      );
      setIsLoading(false);
      setProductSalesCount(salesCount);
      setEmployeeEarning(earning);
    } catch (error) {
      setProductSalesCount('');
      setEmployeeEarning('');
      setIsError(true);
    }
  };

  return (
    <EmployeeItem.Wrapper>
      <EmployeeItem.ActionForm onSubmit={handleCheck}>
        <EmployeeItem.SelectWrapper>
          <Select
            autoFocus
            placeholder="Вибрати працівника"
            value={employee}
            width={260}
            menuWidth={260}
            onChange={val => {
              setEmployee(val);
              setProductSalesCount('');
              setEmployeeEarning('');
            }}
            options={employees.map(val => ({
              label: `${val.name} (${val._id})`,
              value: val._id,
            }))}
          />
        </EmployeeItem.SelectWrapper>
        <Button
          disabled={!employee}
          style={{ width: 120 }}
          type="submit"
          variant="outlined"
        >
          Перевірити
        </Button>
      </EmployeeItem.ActionForm>
      <EmployeeItem.Results isLoading={isLoading}>
        {isLoading && (
          <LinearProgress
            color="secondary"
            style={{ width: '100%', marginRight: 66 }}
          />
        )}
        {productSalesCount !== '' && (
          <EmployeeItem.ResultBox style={{ marginRight: 40 }}>
            <h5 style={{ margin: '0 20px 0 0', width: 168 }}>
              Кількість проданих товарів:
            </h5>
            <p style={{ margin: 0 }}>{productSalesCount}</p>
          </EmployeeItem.ResultBox>
        )}
        {employeeEarning !== '' && (
          <EmployeeItem.ResultBox>
            <h5 style={{ margin: '0 20px 0 0', width: 55 }}>Виручка:</h5>
            <p style={{ margin: 0 }}>{`${employeeEarning} грн.`}</p>
          </EmployeeItem.ResultBox>
        )}
        {isError && <EmployeeItem.ErrorBox>Помилка !</EmployeeItem.ErrorBox>}
      </EmployeeItem.Results>
    </EmployeeItem.Wrapper>
  );
};

EmployeeItem.Wrapper = styled.div`
  display: flex;
`;

EmployeeItem.ActionForm = styled.form`
  width: 420px;
  margin-right: 80px;
`;

EmployeeItem.Results = styled.div`
  width: calc(100vw - 608px);
  display: flex;
  flex-wrap: wrap;
  align-items: ${({ isLoading }) => (isLoading ? 'center' : 'unset')};
`;

EmployeeItem.ResultBox = styled.div`
  display: flex;
  align-items: center;
  line-height: 36px;
`;

EmployeeItem.SelectWrapper = styled.div`
  width: 260px;
  display: inline-block;
  margin-right: 40px;
`;

EmployeeItem.ErrorBox = styled.p`
  padding: 5px 10px;
  margin: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default EmployeeItem;
