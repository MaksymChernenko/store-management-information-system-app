/* eslint-disable no-underscore-dangle */
import { Button, LinearProgress } from '@material-ui/core';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as analysisActions from '../../actions/analysis';
import * as supplierActions from '../../actions/supplier';
import * as supplierSelectors from '../../selectors/supplier';
import * as storeSelectors from '../../selectors/store';

import { Select } from '../../components';

const SupplierItem = () => {
  const dispatch = useDispatch();

  const suppliers = useSelector(supplierSelectors.getSuppliers);
  const store = useSelector(storeSelectors.getStore);

  const [supplier, setSupplier] = useState('');
  const [purchasedProdCost, setPurchasedProdCost] = useState('');
  const [saleProdCost, setSaleProdCost] = useState('');
  const [supplierEff, setSupplierEff] = useState('');

  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (suppliers.length === 0) {
      dispatch(supplierActions.fetchSuppliers({ storeId: store.id }));
    }
  }, []);

  const handleCheck = async e => {
    e.preventDefault();
    setIsError(false);
    try {
      setIsLoading(true);
      const {
        purchasedProductsCost,
        saleProductsCost,
        supplierEfficiency,
      } = await dispatch(
        analysisActions.trackCollaborationWithSuppliers({
          supplierId: supplier.value,
        }),
      );
      setIsLoading(false);
      setPurchasedProdCost(purchasedProductsCost);
      setSaleProdCost(saleProductsCost);
      setSupplierEff(supplierEfficiency);
    } catch (error) {
      setPurchasedProdCost('');
      setSaleProdCost('');
      setSupplierEff('');
      setIsError(true);
    }
  };

  return (
    <SupplierItem.Wrapper>
      <SupplierItem.ActionForm onSubmit={handleCheck}>
        <SupplierItem.SelectWrapper>
          <Select
            autoFocus
            placeholder="Вибрати постачальника"
            value={supplier}
            width={260}
            menuWidth={260}
            onChange={val => {
              setSupplier(val);
              setPurchasedProdCost('');
              setSaleProdCost('');
              setSupplierEff('');
            }}
            options={suppliers.map(val => ({
              label: `${val.name} (${val._id})`,
              value: val._id,
            }))}
          />
        </SupplierItem.SelectWrapper>
        <Button
          disabled={!supplier}
          style={{ width: 120 }}
          type="submit"
          variant="outlined"
        >
          Перевірити
        </Button>
      </SupplierItem.ActionForm>
      <SupplierItem.Results isLoading={isLoading}>
        {isLoading && (
          <LinearProgress
            color="secondary"
            style={{ width: '100%', marginRight: 66 }}
          />
        )}
        {purchasedProdCost !== '' && (
          <SupplierItem.ResultBox style={{ marginRight: 40 }}>
            <h5 style={{ margin: '0 20px 0 0', width: 236 }}>
              Загальна вартість закуплених товарів:
            </h5>
            <p style={{ margin: 0 }}>{`${purchasedProdCost} грн.`}</p>
          </SupplierItem.ResultBox>
        )}
        {saleProdCost !== '' && (
          <SupplierItem.ResultBox style={{ marginRight: 40 }}>
            <h5 style={{ margin: '0 20px 0 0', width: 224 }}>
              Загальна вартість проданих товарів:
            </h5>
            <p style={{ margin: 0 }}>{`${saleProdCost} грн.`}</p>
          </SupplierItem.ResultBox>
        )}
        {supplierEff !== '' && (
          <SupplierItem.ResultBox>
            <h5 style={{ margin: '0 20px 0 0', width: 140 }}>
              Коефіцієнт корисності:
            </h5>
            <p style={{ margin: 0 }}>{supplierEff}</p>
          </SupplierItem.ResultBox>
        )}
        {isError && <SupplierItem.ErrorBox>Помилка !</SupplierItem.ErrorBox>}
      </SupplierItem.Results>
    </SupplierItem.Wrapper>
  );
};

SupplierItem.Wrapper = styled.div`
  display: flex;
`;

SupplierItem.ActionForm = styled.form`
  width: 420px;
  margin-right: 80px;
`;

SupplierItem.Results = styled.div`
  width: calc(100vw - 608px);
  display: flex;
  flex-wrap: wrap;
  align-items: ${({ isLoading }) => (isLoading ? 'center' : 'unset')};
`;

SupplierItem.ResultBox = styled.div`
  display: flex;
  align-items: center;
  line-height: 36px;
`;

SupplierItem.SelectWrapper = styled.div`
  width: 260px;
  display: inline-block;
  margin-right: 40px;
`;

SupplierItem.ErrorBox = styled.p`
  padding: 5px 10px;
  margin: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default SupplierItem;
