import { Button, LinearProgress } from '@material-ui/core';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import * as analysisActions from '../../actions/analysis';

import ProductStackedBarChart from './ProductStackedBarChart';

const buildColumnDefs = () => [
  { headerName: 'ID', field: 'id' },
  { headerName: 'Назва', field: 'name' },
  { headerName: 'Місце', field: 'order' },
  { headerName: 'Кількість продаж', field: 'totalSales' },
  { headerName: 'Постачальник', field: 'supplier' },
];

const ProductsItem = () => {
  const dispatch = useDispatch();

  const [orderedProducts, setOrderedProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const handleCheckProductRate = async () => {
    setOrderedProducts([]);
    setIsError(false);
    try {
      setIsLoading(true);
      const { listOfProductsInOrderOfSale } = await dispatch(
        analysisActions.checkProductsRate(),
      );
      setIsLoading(false);
      setOrderedProducts(listOfProductsInOrderOfSale.slice(0, 10));
    } catch (error) {
      setOrderedProducts([]);
      setIsError(true);
    }
  };

  return (
    <ProductsItem.Wrapper isLoading={isLoading} isError={isError}>
      <Button
        style={{ marginRight: 52 }}
        onClick={handleCheckProductRate}
        variant="outlined"
      >
        Перевірити рейтинг товарів
      </Button>
      {orderedProducts.length > 0 && (
        <div>
          <ProductsItem.GridWrapper className="ag-theme-material">
            <AgGridReact
              columnDefs={buildColumnDefs()}
              rowData={orderedProducts}
              enableFilter
              enableSorting
              enableColResize
              onGridReady={({ api }) => api.sizeColumnsToFit()}
            />
          </ProductsItem.GridWrapper>
          <ProductStackedBarChart data={orderedProducts} />
        </div>
      )}
      {isLoading && (
        <LinearProgress
          color="secondary"
          style={{ width: 'calc(100vw - 442px)' }}
        />
      )}
      {isError && <ProductsItem.ErrorBox>Помилка !</ProductsItem.ErrorBox>}
    </ProductsItem.Wrapper>
  );
};

ProductsItem.Wrapper = styled.div`
  display: flex;
  align-items: ${({ isLoading, isError }) =>
    isLoading || isError ? 'center' : 'self-start'};
`;

ProductsItem.GridWrapper = styled.div`
  height: 316px;
  width: calc(100vw - 444px);
  margin-bottom: 20px;

  & .ag-header-cell {
    background-color: #484c54 !important;
    color: #fff !important;
  }

  & .ag-header-cell-resize {
    background-color: #fff !important;
    width: 5px !important;
    height: 50%;
  }

  & .ag-icon {
    color: #fff !important;
  }
`;

ProductsItem.ErrorBox = styled.p`
  padding: 5px 10px;
  margin: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default ProductsItem;
