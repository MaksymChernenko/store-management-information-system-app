import PropTypes from 'prop-types';

import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts';

const CustomTooltip = ({ active, payload }) => {
  if (active) {
    const { name, totalSales, supplier } = payload[0].payload;
    return (
      <div
        style={{
          padding: '5px 10px',
          backgroundColor: '#373a40',
          color: '#fff',
        }}
      >
        <p>{`Назва: ${name}`}</p>
        <p>{`Постачальник: ${supplier}`}</p>
        <p>{`Кількість продаж: ${totalSales} шт.`}</p>
      </div>
    );
  }

  return null;
};

CustomTooltip.propTypes = {
  active: PropTypes.bool.isRequired,
  payload: PropTypes.array.isRequired,
};

const ProductStackedBarChart = ({ data }) => (
  <BarChart
    width={800}
    height={350}
    data={data}
    margin={{
      top: 20,
      right: 30,
      left: 20,
      bottom: 5,
    }}
  >
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis dataKey="name" />
    <YAxis />
    <Tooltip content={<CustomTooltip />} />
    <Legend />
    <Bar dataKey="totalSales" stackId="a" fill="#484c54" />
  </BarChart>
);

ProductStackedBarChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductStackedBarChart;
