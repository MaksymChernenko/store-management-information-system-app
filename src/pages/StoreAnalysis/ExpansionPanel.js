import PropTypes from 'prop-types';

import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ExpandMore } from '@material-ui/icons';

import React, { useRef, useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';

const useStyles = makeStyles(theme => ({
  panel: {
    marginBottom: 20,
  },
  panelSummary: {
    padding: 20,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

const ExpansionPanelItem = ({
  panelName,
  heading,
  secondaryHeading,
  component: Component,
  index,
  moveCard,
}) => {
  const ref = useRef(null);
  const classes = useStyles();

  const [expanded, setExpanded] = useState(false);
  const handleChange = panel => (_, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const [, drop] = useDrop({
    accept: 'card',
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveCard(dragIndex, hoverIndex);
      // eslint-disable-next-line no-param-reassign
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    item: { type: 'card', id: panelName, index },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  return (
    <ExpansionPanel
      key={panelName}
      className={classes.panel}
      style={{ opacity }}
      expanded={expanded === panelName}
      onChange={handleChange(panelName)}
    >
      <ExpansionPanelSummary
        ref={ref}
        className={classes.panelSummary}
        expandIcon={<ExpandMore />}
        aria-controls={`${panelName}bh-content`}
        id={`${panelName}bh-header`}
      >
        <Typography className={classes.heading}>{heading}</Typography>
        <Typography className={classes.secondaryHeading}>
          {secondaryHeading}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Component />
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

ExpansionPanelItem.propTypes = {
  panelName: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  secondaryHeading: PropTypes.string.isRequired,
  component: PropTypes.node.isRequired,
  index: PropTypes.number.isRequired,
  moveCard: PropTypes.func.isRequired,
};

export default ExpansionPanelItem;
