/* eslint-disable import/prefer-default-export */
import EmployeeItem from './EmployeeItem';
import ProductItem from './ProductItem';
import SupplierItem from './SupplierItem';

export const expansionMenuComponents = {
  employees: EmployeeItem,
  suppliers: SupplierItem,
  products: ProductItem,
};

export const expansionMenuConfig = [
  {
    panelName: 'employees',
    heading: 'Працівники',
    secondaryHeading: 'Відстежити роботу працівників',
  },
  {
    panelName: 'suppliers',
    heading: 'Постачальники',
    secondaryHeading: 'Відстежити співробітництво з постачальниками',
  },
  {
    panelName: 'products',
    heading: 'Товари',
    secondaryHeading: 'Відстежити попит товарів',
  },
];
