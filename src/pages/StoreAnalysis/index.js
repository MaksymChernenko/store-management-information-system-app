import { Breadcrumbs, Chip } from '@material-ui/core';
import { ImportContacts, Menu } from '@material-ui/icons';

import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import styled from 'styled-components';

import { StyledBreadcrumb } from '../../utils';

import ExpansionMenu from './Menu';

const StoreAnalysis = () => {
  const history = useHistory();
  const { id } = useParams();

  const handleGoBack = () => history.push(`/app/store/${id}/store-menu`);

  return (
    <StoreAnalysis.Wrapper>
      <Breadcrumbs
        style={{ position: 'absolute', top: 16, left: 16 }}
        aria-label="breadcrumb"
      >
        <StyledBreadcrumb
          component="button"
          label="Меню"
          icon={<Menu fontSize="small" />}
          onClick={handleGoBack}
        />
        <Chip
          label="Аналіз роботи магазину"
          style={{ backgroundColor: 'inherit' }}
          icon={<ImportContacts fontSize="small" />}
        />
      </Breadcrumbs>
      <StoreAnalysis.Text>Аналіз роботи магазину</StoreAnalysis.Text>
      <ExpansionMenu />
    </StoreAnalysis.Wrapper>
  );
};

StoreAnalysis.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 64px 20px 30px;
`;

StoreAnalysis.Text = styled.p`
  margin: 0 0 30px;
  font-size: 30px;
`;

export default StoreAnalysis;
