/* eslint-disable no-underscore-dangle */
import { CircularProgress } from '@material-ui/core';

import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import update from 'immutability-helper';

import * as menuConfigActions from '../../actions/menuConfig';
import * as menuConfigSelectors from '../../selectors/menuConfig';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { expansionMenuComponents, expansionMenuConfig } from './utils';
import { FETCHING_STATE } from '../../services/reduxHelpers';

import ExpansionPanel from './ExpansionPanel';

const ExpansionMenu = () => {
  const dispatch = useDispatch();

  const analysisMenuConfig = useSelector(
    menuConfigSelectors.getAnalysisMenuConfig,
  );
  const analysisMenuConfigState = useSelector(
    menuConfigSelectors.getAnalysisMenuConfigState,
  );
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [menuConfig, setMenuConfig] = useState([]);

  const getConfig = async () => {
    const { menuConfig: config } = await dispatch(
      menuConfigActions.fetchMenuConfig({
        storeId: store.id,
        userId: user.id,
        configType: 'analysisMenu',
      }),
    );
    if (!config) {
      dispatch(
        menuConfigActions.addMenuConfig({
          configType: 'analysisMenu',
          payload: {
            type: 'analysisMenu',
            menuItems: expansionMenuConfig,
            userId: user.id,
            storeId: store.id,
          },
        }),
      );
    }
  };

  useEffect(() => {
    getConfig();
    return () =>
      dispatch(
        menuConfigActions.clearMenuConfig({ configType: 'analysisMenu' }),
      );
  }, []);

  useEffect(() => {
    if (analysisMenuConfig && menuConfig.length > 0) {
      dispatch(
        menuConfigActions.updateMenuConfig({
          id: analysisMenuConfig._id,
          payload: {
            menuItems: menuConfig,
          },
        }),
      );
    }
  }, [menuConfig]);

  useEffect(() => {
    if (analysisMenuConfig) {
      setMenuConfig(analysisMenuConfig.menuItems);
    }
  }, [analysisMenuConfig]);

  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = menuConfig[dragIndex];
      setMenuConfig(
        update(menuConfig, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        }),
      );
    },
    [menuConfig],
  );

  const renderCard = (
    // eslint-disable-next-line react/prop-types
    { panelName, heading, secondaryHeading },
    index,
  ) => {
    return (
      <ExpansionPanel
        key={panelName}
        index={index}
        panelName={panelName}
        heading={heading}
        secondaryHeading={secondaryHeading}
        component={expansionMenuComponents[panelName]}
        moveCard={moveCard}
      />
    );
  };

  if (analysisMenuConfigState === FETCHING_STATE.FETCHING)
    return (
      <div>
        <CircularProgress />
      </div>
    );

  return <div>{menuConfig.map((item, i) => renderCard(item, i))}</div>;
};

export default ExpansionMenu;
