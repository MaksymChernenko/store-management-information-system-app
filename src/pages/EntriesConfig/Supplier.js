/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { Fab, Tooltip, LinearProgress } from '@material-ui/core';
import { Add, Delete, Edit, Search } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as supplierActions from '../../actions/supplier';
import * as supplierSelectors from '../../selectors/supplier';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import { isAdmin, isCreator } from '../../utils';

import {
  confirm,
  Grid,
  IconCellRenderer,
  Input,
  LoaderOverlay,
  UploadExcelFileInput,
} from '../../components';
import {
  FormGroupTitle,
  FormGroupTitleContainer,
  InfoBlock,
} from '../../components/formComponents';
import SupplierModal from './CreateSupplierModal';

const buildColumnDefs = ({ dispatch, load, name, hasRights }) => {
  const baseColumns = [
    { headerName: 'ID', field: '_id' },
    { headerName: 'Назва', field: 'name', editable: true },
    { headerName: 'Номер телефону', field: 'phoneNumber', editable: true },
    { headerName: 'Місто', field: 'city', editable: true },
  ];
  if (hasRights) {
    baseColumns.unshift({
      headerName: 'Дії',
      field: 'actions',
      cellRendererFramework: IconCellRenderer,
      cellRendererParams: params => ({
        items: [
          {
            icon: Edit,
            onClick: () => {
              const { api, rowIndex } = params;
              const isEdit = api.getEditingCells().length > 0;

              if (isEdit) {
                api.stopEditing();
              } else {
                api.startEditingCell({
                  rowIndex,
                  colKey: 'name',
                });
              }
            },
          },
          {
            icon: Delete,
            onClick: async (_, data) => {
              const { _id } = data;

              if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
                await dispatch(
                  supplierActions.deleteSupplier(
                    { id: _id },
                    `${name} видалив постачальника (${_id})`,
                  ),
                );
                load();
              }
            },
          },
        ],
      }),
    });
  }
  return baseColumns;
};

const Supplier = () => {
  const dispatch = useDispatch();

  const suppliers = useSelector(supplierSelectors.getSuppliers);
  const suppliersState = useSelector(supplierSelectors.getSuppliersState);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [open, setOpen] = useState(false);
  const [gridApi, setGridApi] = useState(null);
  const [search, setSearch] = useState('');

  const [importLoading, setImportLoading] = useState(false);
  const [importError, setImportError] = useState(false);

  const load = () =>
    dispatch(supplierActions.fetchSuppliers({ storeId: store.id }));

  useEffect(() => {
    load();
  }, []);

  useEffect(() => {
    if (gridApi) {
      gridApi.setQuickFilter(search);
    }
  }, [search]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateSupplier = async ({ data }) => {
    const { _id, name, phoneNumber, city } = data;
    await dispatch(
      supplierActions.updateSupplier(
        {
          id: _id,
          payload: { name, phoneNumber, city },
        },
        `${user.name} оновив постачальника (${_id})`,
      ),
    );
    load();
  };

  const pushNewData = async ({ rows, name }) => {
    try {
      setImportLoading(true);
      for (const row of rows) {
        await dispatch(
          supplierActions.createSupplier({
            payload: {
              name: row[0],
              phoneNumber: row[1],
              city: row[2],
              storeId: store.id,
            },
          }),
        );
      }
      await dispatch(
        supplierActions.fetchSuppliers(
          { storeId: store.id },
          `${user.name} виконав імпорт постачальників з файлу ${name}`,
        ),
      );
      setImportLoading(false);
    } catch (error) {
      setImportLoading(false);
      setImportError(true);
    }
  };

  const importExcelData = e => {
    setImportError(false);
    setImportLoading(false);
    const fileObj = e.target.files[0];
    if (fileObj) {
      ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
          setImportError(true);
        } else {
          const rowData = resp.rows.slice(1);
          pushNewData({ rows: rowData, name: fileObj.name });
        }
      });
    }
  };

  const gridReadyHandler = ({ api }) => {
    setGridApi(api);
    api.sizeColumnsToFit();
  };

  return (
    <Supplier.Wrapper>
      <InfoBlock width="100%">
        <FormGroupTitleContainer>
          <FormGroupTitle>Постачальники</FormGroupTitle>
          <Supplier.Container>
            <LoaderOverlay
              isFetching={suppliersState === FETCHING_STATE.FETCHING}
            >
              <Grid
                columnDefs={buildColumnDefs({
                  dispatch,
                  load,
                  name: user.name,
                  hasRights:
                    isCreator(store, user.id) || isAdmin(store, user.id),
                })}
                rowData={suppliers}
                onRowValueChanged={handleUpdateSupplier}
                onGridReady={gridReadyHandler}
              />
            </LoaderOverlay>
            <Input
              placeholder="Пошук"
              style={{
                position: 'absolute',
                top: -48,
                left: '50%',
                width: 150,
                transform: 'translateX(-50%)',
              }}
              value={search}
              onInput={value => setSearch(value)}
              icon={Search}
            />
            <UploadExcelFileInput
              onChange={importExcelData}
              style={{
                fontSize: 12,
                padding: '4px 14px',
                position: 'absolute',
                top: -46,
                left: 'calc(50% + 180px)',
                transform: 'translateX(-50%)',
              }}
            />
            {importLoading && <LinearProgress color="secondary" />}
            {importError && <Supplier.ErrorBox>Помилка!</Supplier.ErrorBox>}
            <Tooltip
              title="Створити постачальника"
              aria-label="create supplier"
            >
              <Fab
                onClick={handleOpen}
                style={{
                  position: 'absolute',
                  top: -38,
                  right: -38,
                  backgroundColor: '#373a40',
                  color: '#fff',
                }}
                aria-label="add"
              >
                <Add />
              </Fab>
            </Tooltip>
          </Supplier.Container>
        </FormGroupTitleContainer>
      </InfoBlock>
      <SupplierModal open={open} handleClose={handleClose} />
    </Supplier.Wrapper>
  );
};

Supplier.Wrapper = styled.div`
  margin-right: 20px;
  padding: 20px;
`;

Supplier.Container = styled.div`
  padding: 20px;
`;

Supplier.ErrorBox = styled.p`
  position: absolute;
  padding: 2px 10px;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  top: -46px;
  left: calc(50% + 318px);
  transform: translateX(-50%);
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default Supplier;
