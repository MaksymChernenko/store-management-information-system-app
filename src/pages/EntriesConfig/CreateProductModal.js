/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import {
  AccountCircle,
  FormatListNumbered,
  MonetizationOn,
} from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as productActions from '../../actions/product';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';
import * as supplierActions from '../../actions/supplier';
import * as supplierSelectors from '../../selectors/supplier';

import { Input, Modal, Select } from '../../components';

const CreateProductModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);
  const suppliers = useSelector(supplierSelectors.getSuppliers);
  const user = useSelector(sessionSelectors.getUser);

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [variableCosts, setVariableCosts] = useState('');
  const [wholesalePrice, setWholesalePrice] = useState('');
  const [count, setCount] = useState('');
  const [supplierId, setSupplierId] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (suppliers.length === 0) {
      dispatch(supplierActions.fetchSuppliers({ storeId: store.id }));
    }
  }, []);

  useEffect(() => {
    setIsError(false);
  }, [name, price, count, supplierId, variableCosts, wholesalePrice]);

  const resetForm = () => {
    setName('');
    setPrice('');
    setCount('');
    setSupplierId('');
    setVariableCosts('');
    setWholesalePrice('');
  };

  const handleCreateProduct = async e => {
    e.preventDefault();
    if (!supplierId) {
      setIsError(true);
      return;
    }
    try {
      await dispatch(
        productActions.createProduct(
          {
            payload: {
              name,
              price,
              count,
              variableCosts,
              wholesalePrice,
              supplierId: supplierId.value,
              storeId: store.id,
            },
          },
          `${user.name} додав новий товар`,
        ),
      );
      await dispatch(productActions.fetchProducts({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreateProductModal.Wrapper>
        <CreateProductModal.Title>Create Product</CreateProductModal.Title>
        <form onSubmit={handleCreateProduct}>
          <CreateProductModal.Container>
            <div style={{ width: 185, marginRight: 30 }}>
              <Input
                label="Назва"
                value={name}
                onInput={value => setName(value)}
                style={{ marginBottom: 20 }}
                icon={AccountCircle}
                autoFocus
                required
              />
              <Input
                label="Ціна"
                value={price}
                onInput={value => setPrice(value)}
                style={{ marginBottom: 20 }}
                icon={MonetizationOn}
                required
                type="number"
              />
              <Input
                label="Оптова ціна"
                value={wholesalePrice}
                onInput={value => setWholesalePrice(value)}
                icon={MonetizationOn}
                required
                type="number"
              />
            </div>
            <div style={{ width: 185 }}>
              <Input
                label="Змінні витрати (на о.т.)"
                value={variableCosts}
                onInput={value => setVariableCosts(value)}
                style={{ marginBottom: 20 }}
                icon={MonetizationOn}
                required
                type="number"
              />
              <Input
                label="Кількість"
                value={count}
                onInput={value => setCount(value)}
                style={{ marginBottom: 20 }}
                icon={FormatListNumbered}
                required
                type="number"
              />
              <div style={{ marginTop: 10 }}>
                <Select
                  placeholder="ID постачальника"
                  width={185}
                  menuWidth={220}
                  value={supplierId}
                  onChange={value => setSupplierId(value)}
                  options={suppliers.map(val => ({
                    label: `${val.name} (${val._id})`,
                    value: val._id,
                  }))}
                />
              </div>
            </div>
            {isError && (
              <CreateProductModal.ErrorBox>
                Помилка !
              </CreateProductModal.ErrorBox>
            )}
          </CreateProductModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreateProductModal.Wrapper>
    </Modal>
  );
};

CreateProductModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreateProductModal.Wrapper = styled.div`
  width: 400px;
`;

CreateProductModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreateProductModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreateProductModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateProductModal;
