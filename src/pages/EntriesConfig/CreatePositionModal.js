import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import { AccountCircle, MonetizationOn } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as positionActions from '../../actions/position';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { Input, Modal } from '../../components';

const CreatePositionModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [name, setName] = useState('');
  const [salary, setSalary] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsError(false);
  }, [name, salary]);

  const resetForm = () => {
    setName('');
    setSalary('');
  };

  const handleCreatePosition = async e => {
    e.preventDefault();
    try {
      await dispatch(
        positionActions.createPosition(
          { payload: { name, salary, storeId: store.id } },
          `${user.name} додав нову посаду`,
        ),
      );
      await dispatch(positionActions.fetchPositions({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreatePositionModal.Wrapper>
        <CreatePositionModal.Title>Створити посаду</CreatePositionModal.Title>
        <form onSubmit={handleCreatePosition}>
          <CreatePositionModal.Container>
            <Input
              label="Назва"
              style={{ width: 185 }}
              value={name}
              onInput={value => setName(value)}
              icon={AccountCircle}
              autoFocus
              required
            />
            <Input
              label="Зарплата"
              style={{ width: 185 }}
              value={salary}
              onInput={value => setSalary(value)}
              icon={MonetizationOn}
              required
              type="number"
            />
            {isError && (
              <CreatePositionModal.ErrorBox>
                Помилка !
              </CreatePositionModal.ErrorBox>
            )}
          </CreatePositionModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreatePositionModal.Wrapper>
    </Modal>
  );
};

CreatePositionModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreatePositionModal.Wrapper = styled.div`
  width: 400px;
`;

CreatePositionModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreatePositionModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreatePositionModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreatePositionModal;
