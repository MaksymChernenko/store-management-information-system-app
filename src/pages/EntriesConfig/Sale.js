/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { Fab, Tooltip, LinearProgress } from '@material-ui/core';
import { Add, Delete, Edit, Search } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as saleActions from '../../actions/sale';
import * as saleSelectors from '../../selectors/sale';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import { dateFormatter, paymentTypes, isAdmin, isCreator } from '../../utils';

import {
  confirm,
  Grid,
  IconCellRenderer,
  Input,
  LoaderOverlay,
  UploadExcelFileInput,
} from '../../components';
import {
  FormGroupTitle,
  FormGroupTitleContainer,
  InfoBlock,
} from '../../components/formComponents';
import SaleModal from './CreateSaleModal';

const buildColumnDefs = ({ dispatch, load, name, hasRights }) => {
  const baseColumns = [
    { headerName: 'ID', field: '_id' },
    { headerName: 'ID працівника', field: 'employeeId' },
    { headerName: 'ID товару', field: 'productId' },
    { headerName: 'Дата', field: 'date', valueFormatter: dateFormatter },
    { headerName: 'Кількість', field: 'count', editable: true },
    { headerName: 'Ціна', field: 'price', editable: true },
    {
      headerName: 'Тип оплати',
      field: 'paymentType',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
        values: paymentTypes,
      },
    },
  ];
  if (hasRights) {
    baseColumns.unshift({
      headerName: 'Дії',
      field: 'actions',
      cellRendererFramework: IconCellRenderer,
      cellRendererParams: params => ({
        items: [
          {
            icon: Edit,
            onClick: () => {
              const { api, rowIndex } = params;
              const isEdit = api.getEditingCells().length > 0;

              if (isEdit) {
                api.stopEditing();
              } else {
                api.startEditingCell({
                  rowIndex,
                  colKey: 'count',
                });
              }
            },
          },
          {
            icon: Delete,
            onClick: async (_, data) => {
              const { _id } = data;

              if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
                await dispatch(
                  saleActions.deleteSale(
                    { id: _id },
                    `${name} видалив продажу (${_id})`,
                  ),
                );
                load();
              }
            },
          },
        ],
      }),
    });
  }
  return baseColumns;
};

const Sale = () => {
  const dispatch = useDispatch();

  const sales = useSelector(saleSelectors.getSales);
  const salesState = useSelector(saleSelectors.getSalesState);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [open, setOpen] = useState(false);
  const [gridApi, setGridApi] = useState(null);
  const [search, setSearch] = useState('');

  const [importLoading, setImportLoading] = useState(false);
  const [importError, setImportError] = useState(false);

  const load = () => dispatch(saleActions.fetchSales({ storeId: store.id }));

  useEffect(() => {
    load();
  }, []);

  useEffect(() => {
    if (gridApi) {
      gridApi.setQuickFilter(search);
    }
  }, [search]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateSale = async ({ data }) => {
    const { _id, price, count, paymentType } = data;
    await dispatch(
      saleActions.updateSale(
        {
          id: _id,
          payload: { price, count, paymentType },
        },
        `${user.name} оновив продажу (${_id})`,
      ),
    );
    load();
  };

  const pushNewData = async ({ rows, name }) => {
    try {
      setImportLoading(true);
      for (const row of rows) {
        await dispatch(
          saleActions.createSale({
            payload: {
              employeeId: row[0],
              productId: row[1],
              date: row[2],
              paymentType: row[3],
              count: row[4],
              price: row[5],
              storeId: store.id,
            },
          }),
        );
      }
      await dispatch(
        saleActions.fetchSales(
          { storeId: store.id },
          `${user.name} виконав імпорт продаж з файлу ${name}`,
        ),
      );
      setImportLoading(false);
    } catch (error) {
      setImportLoading(false);
      setImportError(true);
    }
  };

  const importExcelData = e => {
    setImportError(false);
    setImportLoading(false);
    const fileObj = e.target.files[0];
    if (fileObj) {
      ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
          setImportError(true);
        } else {
          const rowData = resp.rows.slice(1);
          pushNewData({ rows: rowData, name: fileObj.name });
        }
      });
    }
  };

  const gridReadyHandler = ({ api }) => {
    setGridApi(api);
    api.sizeColumnsToFit();
  };

  return (
    <Sale.Wrapper>
      <InfoBlock width="100%">
        <FormGroupTitleContainer>
          <FormGroupTitle>Продажі</FormGroupTitle>
          <Sale.Container>
            <LoaderOverlay isFetching={salesState === FETCHING_STATE.FETCHING}>
              <Grid
                columnDefs={buildColumnDefs({
                  dispatch,
                  load,
                  name: user.name,
                  hasRights:
                    isCreator(store, user.id) || isAdmin(store, user.id),
                })}
                rowData={sales}
                onRowValueChanged={handleUpdateSale}
                onGridReady={gridReadyHandler}
              />
            </LoaderOverlay>
            <Input
              placeholder="Пошук"
              style={{
                position: 'absolute',
                top: -48,
                left: '50%',
                width: 150,
                transform: 'translateX(-50%)',
              }}
              value={search}
              onInput={value => setSearch(value)}
              icon={Search}
            />
            <UploadExcelFileInput
              onChange={importExcelData}
              style={{
                fontSize: 12,
                padding: '4px 14px',
                position: 'absolute',
                top: -46,
                left: 'calc(50% + 180px)',
                transform: 'translateX(-50%)',
              }}
            />
            {importLoading && <LinearProgress color="secondary" />}
            {importError && <Sale.ErrorBox>Помилка!</Sale.ErrorBox>}
            <Tooltip title="Створити продажу" aria-label="create sale">
              <Fab
                onClick={handleOpen}
                style={{
                  position: 'absolute',
                  top: -38,
                  right: -38,
                  backgroundColor: '#373a40',
                  color: '#fff',
                }}
                aria-label="add"
              >
                <Add />
              </Fab>
            </Tooltip>
          </Sale.Container>
        </FormGroupTitleContainer>
      </InfoBlock>
      <SaleModal open={open} handleClose={handleClose} />
    </Sale.Wrapper>
  );
};

Sale.Wrapper = styled.div`
  margin-right: 20px;
  padding: 20px;
`;

Sale.Container = styled.div`
  padding: 20px;
`;

Sale.ErrorBox = styled.p`
  position: absolute;
  padding: 2px 10px;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  top: -46px;
  left: calc(50% + 318px);
  transform: translateX(-50%);
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default Sale;
