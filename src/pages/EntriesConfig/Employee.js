/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { Fab, Tooltip, LinearProgress } from '@material-ui/core';
import { Add, Delete, Edit, Search } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as employeeActions from '../../actions/employee';
import * as employeeSelectors from '../../selectors/employee';
import * as positionActions from '../../actions/position';
import * as positionSelectors from '../../selectors/position';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import { isAdmin, isCreator } from '../../utils';

import {
  confirm,
  Grid,
  IconCellRenderer,
  Input,
  LoaderOverlay,
  UploadExcelFileInput,
} from '../../components';
import {
  FormGroupTitle,
  FormGroupTitleContainer,
  InfoBlock,
} from '../../components/formComponents';
import EmployeeModal from './CreateEmployeeModal';

const buildColumnDefs = ({
  dispatch,
  load,
  positions = [],
  name,
  hasRights,
}) => {
  const baseColumns = [
    { headerName: 'ID', field: '_id' },
    { headerName: `Ім'я`, field: 'name', editable: true },
    { headerName: 'Фамілія', field: 'surname', editable: true },
    {
      headerName: 'Посада',
      field: 'position',
      editable: true,
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {
        values: positions,
      },
    },
    { headerName: 'Номер телефону', field: 'phoneNumber', editable: true },
    { headerName: 'Зарплата', field: 'salary', editable: true },
  ];

  if (hasRights) {
    baseColumns.unshift({
      headerName: 'Дії',
      field: 'actions',
      cellRendererFramework: IconCellRenderer,
      cellRendererParams: params => ({
        items: [
          {
            icon: Edit,
            onClick: () => {
              const { api, rowIndex } = params;
              const isEdit = api.getEditingCells().length > 0;

              if (isEdit) {
                api.stopEditing();
              } else {
                api.startEditingCell({
                  rowIndex,
                  colKey: 'name',
                });
              }
            },
          },
          {
            icon: Delete,
            onClick: async (_, data) => {
              const { _id } = data;

              if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
                await dispatch(
                  employeeActions.deleteEmployee(
                    { id: _id },
                    `${name} видалив працівника (${_id})`,
                  ),
                );
                load();
              }
            },
          },
        ],
      }),
    });
  }
  return baseColumns;
};

const Employee = () => {
  const dispatch = useDispatch();

  const employees = useSelector(employeeSelectors.getEmployees);
  const employeesState = useSelector(employeeSelectors.getEmployeesState);
  const positions = useSelector(positionSelectors.getPositions);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [open, setOpen] = useState(false);
  const [gridApi, setGridApi] = useState(null);
  const [search, setSearch] = useState('');

  const [importLoading, setImportLoading] = useState(false);
  const [importError, setImportError] = useState(false);

  const load = () =>
    dispatch(employeeActions.fetchEmployees({ storeId: store.id }));

  useEffect(() => {
    if (positions.length === 0) {
      dispatch(positionActions.fetchPositions({ storeId: store.id })).then(() =>
        load(),
      );
    } else load();
  }, []);

  useEffect(() => {
    if (gridApi) {
      gridApi.setQuickFilter(search);
    }
  }, [search]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateEmployee = async ({ data }) => {
    const { _id, name, surname, position, phoneNumber, salary } = data;
    await dispatch(
      employeeActions.updateEmployee(
        {
          id: _id,
          payload: { name, surname, position, phoneNumber, salary },
        },
        `${user.name} оновив працівника (${_id})`,
      ),
    );
    load();
  };

  const pushNewData = async ({ rows, name }) => {
    try {
      setImportLoading(true);
      for (const row of rows) {
        await dispatch(
          employeeActions.createEmployee({
            payload: {
              name: row[0],
              surname: row[1],
              position: row[2],
              phoneNumber: row[3],
              salary: row[4],
              storeId: store.id,
            },
          }),
        );
      }
      await dispatch(
        employeeActions.fetchEmployees(
          { storeId: store.id },
          `${user.name} виконав імпорт працівників з файлу ${name}`,
        ),
      );
      setImportLoading(false);
    } catch (error) {
      setImportError(true);
    }
  };

  const importExcelData = e => {
    setImportError(false);
    setImportLoading(false);
    const fileObj = e.target.files[0];
    if (fileObj) {
      ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
          setImportError(true);
        } else {
          const rowData = resp.rows.slice(1);
          pushNewData({ rows: rowData, name: fileObj.name });
        }
      });
    }
  };

  const gridReadyHandler = ({ api }) => {
    setGridApi(api);
    api.sizeColumnsToFit();
  };

  return (
    <Employee.Wrapper>
      <InfoBlock width="100%">
        <FormGroupTitleContainer>
          <FormGroupTitle>Працівники</FormGroupTitle>
          <Employee.Container>
            {positions.length > 0 && (
              <LoaderOverlay
                isFetching={employeesState === FETCHING_STATE.FETCHING}
              >
                <Grid
                  columnDefs={buildColumnDefs({
                    dispatch,
                    load,
                    positions: positions.map(pos => pos.name),
                    name: user.name,
                    hasRights:
                      isCreator(store, user.id) || isAdmin(store, user.id),
                  })}
                  rowData={employees}
                  onRowValueChanged={handleUpdateEmployee}
                  onGridReady={gridReadyHandler}
                />
              </LoaderOverlay>
            )}
            <Input
              placeholder="Пошук"
              style={{
                position: 'absolute',
                top: -48,
                left: '50%',
                width: 150,
                transform: 'translateX(-50%)',
              }}
              value={search}
              onInput={value => setSearch(value)}
              icon={Search}
            />
            <UploadExcelFileInput
              onChange={importExcelData}
              style={{
                fontSize: 12,
                padding: '4px 14px',
                position: 'absolute',
                top: -46,
                left: 'calc(50% + 180px)',
                transform: 'translateX(-50%)',
              }}
            />
            {importLoading && <LinearProgress color="secondary" />}
            {importError && <Employee.ErrorBox>Помилка!</Employee.ErrorBox>}
            <Tooltip title="Створити працівника" aria-label="create employee">
              <Fab
                onClick={handleOpen}
                style={{
                  position: 'absolute',
                  top: -38,
                  right: -38,
                  backgroundColor: '#373a40',
                  color: '#fff',
                }}
                aria-label="add"
              >
                <Add />
              </Fab>
            </Tooltip>
          </Employee.Container>
        </FormGroupTitleContainer>
      </InfoBlock>
      <EmployeeModal open={open} handleClose={handleClose} />
    </Employee.Wrapper>
  );
};

Employee.Wrapper = styled.div`
  margin-right: 20px;
  padding: 20px;
`;

Employee.Container = styled.div`
  padding: 20px;
`;

Employee.ErrorBox = styled.p`
  position: absolute;
  padding: 2px 10px;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  top: -46px;
  left: calc(50% + 318px);
  transform: translateX(-50%);
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default Employee;
