import { Tab, Tabs } from '@material-ui/core';
import {
  EmojiPeople,
  Extension,
  FolderShared,
  MonetizationOn,
  Subject,
  SupervisedUserCircle,
} from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import * as employeeActions from '../../actions/employee';
import * as positionActions from '../../actions/position';
import * as productActions from '../../actions/product';
import * as saleActions from '../../actions/sale';
import * as supplierActions from '../../actions/supplier';
import * as waybillActions from '../../actions/waybill';

import { a11yTabProps, useVerticalTabStyles } from '../../utils';

import TabPanel from '../../components/TabPanel';
import Employee from './Employee';
import Position from './Position';
import Product from './Product';
import Sale from './Sale';
import Supplier from './Supplier';
import Waybill from './Waybill';

const ConfigMenu = () => {
  const classes = useVerticalTabStyles();
  const dispatch = useDispatch();

  const [value, setValue] = useState(0);

  useEffect(() => {
    return () => {
      dispatch(employeeActions.resetEmployees());
      dispatch(positionActions.resetPositions());
      dispatch(productActions.resetProducts());
      dispatch(saleActions.resetSales());
      dispatch(supplierActions.resetSuppliers());
      dispatch(waybillActions.resetWaybills());
    };
  }, []);

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs"
        className={classes.tabs}
      >
        <Tab
          className={classes.tab}
          icon={<EmojiPeople />}
          label="Працівники"
          {...a11yTabProps(0)}
        />
        <Tab
          className={classes.tab}
          icon={<FolderShared />}
          label="Посади"
          {...a11yTabProps(1)}
        />
        <Tab
          className={classes.tab}
          icon={<Extension />}
          label="Товари"
          {...a11yTabProps(2)}
        />
        <Tab
          className={classes.tab}
          icon={<MonetizationOn />}
          label="Продажі"
          {...a11yTabProps(3)}
        />
        <Tab
          className={classes.tab}
          icon={<SupervisedUserCircle />}
          label="Постачальники"
          {...a11yTabProps(4)}
        />
        <Tab
          className={classes.tab}
          icon={<Subject />}
          label="Накладні"
          {...a11yTabProps(5)}
        />
      </Tabs>
      <TabPanel value={value} index={0}>
        <Employee />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Position />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Product />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Sale />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Supplier />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <Waybill />
      </TabPanel>
    </div>
  );
};

export default ConfigMenu;
