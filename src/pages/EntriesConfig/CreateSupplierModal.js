import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import { AccountCircle, LocationCity, Phone } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as supplierActions from '../../actions/supplier';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { Input, Modal } from '../../components';

const CreateSupplierModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [city, setCity] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsError(false);
  }, [name, phoneNumber, city]);

  const resetForm = () => {
    setName('');
    setPhoneNumber('');
    setCity('');
  };

  const handleCreateSupplier = async e => {
    e.preventDefault();
    try {
      await dispatch(
        supplierActions.createSupplier(
          {
            payload: {
              name,
              phoneNumber,
              city,
              storeId: store.id,
            },
          },
          `${user.name} додав нового постачальника`,
        ),
      );
      await dispatch(supplierActions.fetchSuppliers({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreateSupplierModal.Wrapper>
        <CreateSupplierModal.Title>
          Створити постачальника
        </CreateSupplierModal.Title>
        <form onSubmit={handleCreateSupplier}>
          <CreateSupplierModal.Container>
            <div style={{ width: 185, marginRight: 30 }}>
              <Input
                label="Назва"
                value={name}
                onInput={value => setName(value)}
                style={{ marginBottom: 20 }}
                icon={AccountCircle}
                autoFocus
                required
              />
              <Input
                label="Номер телефону"
                value={phoneNumber}
                onInput={value => setPhoneNumber(value)}
                icon={Phone}
                mask="mobilePhone"
                required
              />
            </div>
            <div style={{ width: 185 }}>
              <Input
                label="Місто"
                value={city}
                onInput={value => setCity(value)}
                icon={LocationCity}
                required
              />
            </div>
            {isError && (
              <CreateSupplierModal.ErrorBox>
                Помилка !
              </CreateSupplierModal.ErrorBox>
            )}
          </CreateSupplierModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreateSupplierModal.Wrapper>
    </Modal>
  );
};

CreateSupplierModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreateSupplierModal.Wrapper = styled.div`
  width: 400px;
`;

CreateSupplierModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreateSupplierModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreateSupplierModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateSupplierModal;
