/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import {
  CalendarToday,
  FormatListNumbered,
  MonetizationOn,
} from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as employeeActions from '../../actions/employee';
import * as employeeSelectors from '../../selectors/employee';
import * as productActions from '../../actions/product';
import * as productSelectors from '../../selectors/product';
import * as saleActions from '../../actions/sale';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { paymentTypes } from '../../utils';

import { Input, Modal, Select } from '../../components';

const CreateSaleModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const employees = useSelector(employeeSelectors.getEmployees);
  const products = useSelector(productSelectors.getProducts);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [employeeId, setEmployeeId] = useState('');
  const [productId, setProductId] = useState('');
  const [date, setDate] = useState('');
  const [paymentType, setPaymentType] = useState('');
  const [count, setCount] = useState('');
  const [price, setPrice] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (employees.length === 0) {
      dispatch(employeeActions.fetchEmployees({ storeId: store.id }));
    }
    if (products.length === 0) {
      dispatch(productActions.fetchProducts({ storeId: store.id }));
    }
  }, []);

  useEffect(() => {
    setIsError(false);
  }, [employeeId, productId, date, paymentType, price, count]);

  const resetForm = () => {
    setEmployeeId('');
    setProductId('');
    setDate('');
    setPaymentType('');
    setPrice('');
    setCount('');
  };

  const handleCreateSale = async e => {
    e.preventDefault();
    if (!employeeId && !productId && !paymentType) {
      setIsError(true);
      return;
    }
    try {
      await dispatch(
        saleActions.createSale(
          {
            payload: {
              employeeId: employeeId.value,
              productId: productId.value,
              date,
              paymentType: paymentType.value,
              count,
              price,
              storeId: store.id,
            },
          },
          `${user.name} додав нову продажу`,
        ),
      );
      await dispatch(saleActions.fetchSales({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreateSaleModal.Wrapper>
        <CreateSaleModal.Title>Створити продажу</CreateSaleModal.Title>
        <form onSubmit={handleCreateSale}>
          <CreateSaleModal.Container>
            <div style={{ width: 185, marginRight: 30 }}>
              <div style={{ margin: '10px 0 30px' }}>
                <Select
                  autoFocus
                  placeholder="ID працівника"
                  width={185}
                  menuWidth={220}
                  value={employeeId}
                  onChange={value => setEmployeeId(value)}
                  options={employees.map(val => ({
                    label: `${val.name} (${val._id})`,
                    value: val._id,
                  }))}
                />
              </div>
              <div style={{ marginBottom: 30 }}>
                <Select
                  placeholder="ID товару"
                  width={185}
                  menuWidth={220}
                  value={productId}
                  onChange={value => {
                    setProductId(value);
                    const productPrice = products.find(
                      ({ _id }) => _id === value.value,
                    ).price;
                    setPrice(productPrice);
                  }}
                  options={products.map(val => ({
                    label: `${val.name} (${val._id})`,
                    value: val._id,
                  }))}
                />
              </div>
              <Select
                placeholder="Тип оплати"
                width={185}
                value={paymentType}
                onChange={value => setPaymentType(value)}
                options={paymentTypes.map(val => ({
                  label: val,
                  value: val,
                }))}
              />
            </div>
            <div style={{ width: 185 }}>
              <Input
                label="Кількість"
                style={{ marginBottom: 20 }}
                value={count}
                onInput={value => setCount(value)}
                icon={FormatListNumbered}
                type="number"
                required
              />
              <Input
                label="Ціна"
                style={{ marginBottom: 20 }}
                value={price}
                onInput={value => setPrice(value)}
                icon={MonetizationOn}
                type="number"
                required
              />
              <Input
                label="Дата"
                style={{ width: 185 }}
                value={date}
                onInput={value => setDate(value)}
                icon={CalendarToday}
                type="date"
                required
              />
            </div>
            {isError && (
              <CreateSaleModal.ErrorBox>Помилка !</CreateSaleModal.ErrorBox>
            )}
          </CreateSaleModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreateSaleModal.Wrapper>
    </Modal>
  );
};

CreateSaleModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreateSaleModal.Wrapper = styled.div`
  width: 400px;
`;

CreateSaleModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreateSaleModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreateSaleModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateSaleModal;
