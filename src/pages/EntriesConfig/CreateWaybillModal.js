/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import { FormatListNumbered, MonetizationOn } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as productSelectors from '../../selectors/product';
import * as productActions from '../../actions/product';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';
import * as waybillActions from '../../actions/waybill';

import { Input, Modal, Select } from '../../components';

const CreateWaybillModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const products = useSelector(productSelectors.getProducts);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [productId, setProductId] = useState('');
  const [price, setPrice] = useState('');
  const [count, setCount] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (products.length === 0) {
      dispatch(productActions.fetchProducts({ storeId: store.id }));
    }
  }, []);

  useEffect(() => {
    setIsError(false);
  }, [productId, price, count]);

  const resetForm = () => {
    setProductId('');
    setPrice('');
    setCount('');
  };

  const handleCreateWaybill = async e => {
    e.preventDefault();
    if (!productId) {
      setIsError(true);
      return;
    }
    try {
      await dispatch(
        waybillActions.createWaybill(
          {
            payload: {
              productId: productId.value,
              price,
              count,
              storeId: store.id,
            },
          },
          `${user.name} додав нову накладну`,
        ),
      );
      await dispatch(waybillActions.fetchWaybills({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreateWaybillModal.Wrapper>
        <CreateWaybillModal.Title>Створити накладну</CreateWaybillModal.Title>
        <form onSubmit={handleCreateWaybill}>
          <CreateWaybillModal.Container>
            <div style={{ width: 185, marginRight: 30 }}>
              <div style={{ margin: '10px 0 20px' }}>
                <Select
                  placeholder="ID товару"
                  width={185}
                  menuWidth={220}
                  value={productId}
                  onChange={value => setProductId(value)}
                  options={products.map(val => ({
                    label: `${val.name} (${val._id})`,
                    value: val._id,
                  }))}
                  autoFocus
                />
              </div>
              <Input
                label="Ціна"
                value={price}
                onInput={value => setPrice(value)}
                icon={MonetizationOn}
                type="number"
                required
              />
            </div>
            <div style={{ width: 185 }}>
              <Input
                label="Кількість"
                value={count}
                onInput={value => setCount(value)}
                icon={FormatListNumbered}
                type="number"
                required
              />
            </div>
            {isError && (
              <CreateWaybillModal.ErrorBox>
                Помилка !
              </CreateWaybillModal.ErrorBox>
            )}
          </CreateWaybillModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreateWaybillModal.Wrapper>
    </Modal>
  );
};

CreateWaybillModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreateWaybillModal.Wrapper = styled.div`
  width: 400px;
`;

CreateWaybillModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreateWaybillModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreateWaybillModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateWaybillModal;
