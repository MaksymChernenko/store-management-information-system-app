/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import { Fab, Tooltip, LinearProgress } from '@material-ui/core';
import { Add, Delete, Edit, Search } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as productActions from '../../actions/product';
import * as productSelectors from '../../selectors/product';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import { isAdmin, isCreator } from '../../utils';

import {
  confirm,
  Grid,
  IconCellRenderer,
  Input,
  LoaderOverlay,
  UploadExcelFileInput,
} from '../../components';
import {
  FormGroupTitle,
  FormGroupTitleContainer,
  InfoBlock,
} from '../../components/formComponents';
import ProductModal from './CreateProductModal';

const buildColumnDefs = ({ dispatch, load, name, hasRights }) => {
  const baseColumns = [
    { headerName: 'ID', field: '_id' },
    { headerName: 'ID постачальника', field: 'supplierId' },
    { headerName: 'Назва', field: 'name', editable: true },
    { headerName: 'Ціна', field: 'price', editable: true },
    {
      headerName: 'Змінні витрати (на о.о.)',
      field: 'variableCosts',
      editable: true,
    },
    { headerName: 'Оптова Ціна', field: 'wholesalePrice', editable: true },
    { headerName: 'Кількість', field: 'count', editable: true },
  ];
  if (hasRights) {
    baseColumns.unshift({
      headerName: 'Дії',
      field: 'actions',
      cellRendererFramework: IconCellRenderer,
      cellRendererParams: params => ({
        items: [
          {
            icon: Edit,
            onClick: () => {
              const { api, rowIndex } = params;
              const isEdit = api.getEditingCells().length > 0;

              if (isEdit) {
                api.stopEditing();
              } else {
                api.startEditingCell({
                  rowIndex,
                  colKey: 'name',
                });
              }
            },
          },
          {
            icon: Delete,
            onClick: async (_, data) => {
              const { _id } = data;

              if (await confirm('Ви впевнені ?', { action: 'Видалення' })) {
                await dispatch(
                  productActions.deleteProduct(
                    { id: _id },
                    `${name} видалив товар (${_id})`,
                  ),
                );
                load();
              }
            },
          },
        ],
      }),
    });
  }
  return baseColumns;
};

const Product = () => {
  const dispatch = useDispatch();

  const products = useSelector(productSelectors.getProducts);
  const productsState = useSelector(productSelectors.getProductsState);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [open, setOpen] = useState(false);
  const [gridApi, setGridApi] = useState(null);
  const [search, setSearch] = useState('');

  const [importLoading, setImportLoading] = useState(false);
  const [importError, setImportError] = useState(false);

  const load = () =>
    dispatch(productActions.fetchProducts({ storeId: store.id }));

  useEffect(() => {
    load();
  }, []);

  useEffect(() => {
    if (gridApi) {
      gridApi.setQuickFilter(search);
    }
  }, [search]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateProduct = async ({ data }) => {
    const { _id, name, price, count, variableCosts, wholesalePrice } = data;
    await dispatch(
      productActions.updateProduct(
        {
          id: _id,
          payload: { name, price, count, wholesalePrice, variableCosts },
        },
        `${user.name} оновив товар (${_id})`,
      ),
    );
    load();
  };

  const pushNewData = async ({ rows, name }) => {
    try {
      setImportLoading(true);
      for (const row of rows) {
        await dispatch(
          productActions.createProduct({
            payload: {
              name: row[0],
              price: row[1],
              count: row[2],
              variableCosts: row[3],
              wholesalePrice: row[4],
              supplierId: row[5],
              storeId: store.id,
            },
          }),
        );
      }
      await dispatch(
        productActions.fetchProducts(
          { storeId: store.id },
          `${user.name} виконав імпорт товарів з файлу ${name}`,
        ),
      );
      setImportLoading(false);
    } catch (error) {
      setImportLoading(false);
      setImportError(true);
    }
  };

  const importExcelData = e => {
    setImportError(false);
    setImportLoading(false);
    const fileObj = e.target.files[0];
    if (fileObj) {
      ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
          setImportError(true);
        } else {
          const rowData = resp.rows.slice(1);
          pushNewData({ rows: rowData, name: fileObj.name });
        }
      });
    }
  };

  const gridReadyHandler = ({ api }) => {
    setGridApi(api);
    api.sizeColumnsToFit();
  };

  return (
    <Product.Wrapper>
      <InfoBlock width="100%">
        <FormGroupTitleContainer>
          <FormGroupTitle>Товари</FormGroupTitle>
          <Product.Container>
            <LoaderOverlay
              isFetching={productsState === FETCHING_STATE.FETCHING}
            >
              <Grid
                columnDefs={buildColumnDefs({
                  dispatch,
                  load,
                  name: user.name,
                  hasRights:
                    isCreator(store, user.id) || isAdmin(store, user.id),
                })}
                rowData={products}
                onRowValueChanged={handleUpdateProduct}
                onGridReady={gridReadyHandler}
              />
            </LoaderOverlay>
            <Input
              placeholder="Пошук"
              style={{
                position: 'absolute',
                top: -48,
                left: '50%',
                width: 150,
                transform: 'translateX(-50%)',
              }}
              value={search}
              onInput={value => setSearch(value)}
              icon={Search}
            />
            <UploadExcelFileInput
              onChange={importExcelData}
              style={{
                fontSize: 12,
                padding: '4px 14px',
                position: 'absolute',
                top: -46,
                left: 'calc(50% + 180px)',
                transform: 'translateX(-50%)',
              }}
            />
            {importLoading && <LinearProgress color="secondary" />}
            {importError && <Product.ErrorBox>Помилка!</Product.ErrorBox>}
            <Tooltip title="Створити товар" aria-label="create product">
              <Fab
                onClick={handleOpen}
                style={{
                  position: 'absolute',
                  top: -38,
                  right: -38,
                  backgroundColor: '#373a40',
                  color: '#fff',
                }}
                aria-label="add"
              >
                <Add />
              </Fab>
            </Tooltip>
          </Product.Container>
        </FormGroupTitleContainer>
      </InfoBlock>
      <ProductModal open={open} handleClose={handleClose} />
    </Product.Wrapper>
  );
};

Product.Wrapper = styled.div`
  margin-right: 20px;
  padding: 20px;
`;

Product.Container = styled.div`
  padding: 20px;
`;

Product.ErrorBox = styled.p`
  position: absolute;
  padding: 2px 10px;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  top: -46px;
  left: calc(50% + 318px);
  transform: translateX(-50%);
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default Product;
