/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import { AccountCircle, MonetizationOn, Phone } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as employeeActions from '../../actions/employee';
import * as positionActions from '../../actions/position';
import * as positionSelectors from '../../selectors/position';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { Input, Modal, Select } from '../../components';

const CreateEmployeeModal = ({ handleClose, open }) => {
  const dispatch = useDispatch();

  const positions = useSelector(positionSelectors.getPositions);
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [position, setPosition] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [salary, setSalary] = useState('');

  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (positions.length === 0) {
      dispatch(positionActions.fetchPositions({ storeId: store.id }));
    }
  }, []);

  useEffect(() => {
    setIsError(false);
  }, [name, surname, position, phoneNumber, salary]);

  const resetForm = () => {
    setName('');
    setSurname('');
    setPosition('');
    setPhoneNumber('');
    setSalary('');
  };

  const handleCreateEmployee = async e => {
    e.preventDefault();
    if (!position) {
      setIsError(true);
      return;
    }
    try {
      await dispatch(
        employeeActions.createEmployee(
          {
            payload: {
              name,
              surname,
              position: position.value,
              phoneNumber,
              salary,
              storeId: store.id,
            },
          },
          `${user.name} додав нового працівника`,
        ),
      );
      await dispatch(employeeActions.fetchEmployees({ storeId: store.id }));
      resetForm();
      handleClose();
    } catch (err) {
      setIsError(true);
    }
  };

  return (
    <Modal open={open} handleClose={handleClose}>
      <CreateEmployeeModal.Wrapper>
        <CreateEmployeeModal.Title>
          Створити працівника
        </CreateEmployeeModal.Title>
        <form onSubmit={handleCreateEmployee}>
          <CreateEmployeeModal.Container>
            <div style={{ width: 185, marginRight: 30 }}>
              <Input
                label="Ім'я"
                value={name}
                onInput={value => setName(value)}
                style={{ marginBottom: 20 }}
                icon={AccountCircle}
                autoFocus
                required
              />
              <Input
                label="Фамілія"
                value={surname}
                onInput={value => setSurname(value)}
                style={{ marginBottom: 20 }}
                icon={AccountCircle}
                required
              />
              <Input
                label="Номер телефону"
                value={phoneNumber}
                onInput={value => setPhoneNumber(value)}
                icon={Phone}
                mask="mobilePhone"
                required
              />
            </div>
            <div style={{ width: 185 }}>
              <div style={{ margin: '10px 0 20px' }}>
                <Select
                  placeholder="Посада"
                  width={185}
                  value={position}
                  onChange={value => {
                    setPosition(value);
                    const baseSalary = positions.find(
                      pos => pos.name === value.value,
                    ).salary;
                    setSalary(baseSalary);
                  }}
                  options={positions.map(val => ({
                    label: val.name,
                    value: val.name,
                  }))}
                />
              </div>
              <Input
                label="Зарплата"
                value={salary}
                onInput={value => setSalary(value)}
                icon={MonetizationOn}
                required
                type="number"
              />
            </div>
            {isError && (
              <CreateEmployeeModal.ErrorBox>
                Помилка !
              </CreateEmployeeModal.ErrorBox>
            )}
          </CreateEmployeeModal.Container>
          <Button
            style={{ display: 'block', marginLeft: 'auto' }}
            variant="outlined"
            type="submit"
          >
            Створити
          </Button>
        </form>
      </CreateEmployeeModal.Wrapper>
    </Modal>
  );
};

CreateEmployeeModal.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
};

CreateEmployeeModal.Wrapper = styled.div`
  width: 400px;
`;

CreateEmployeeModal.Title = styled.p`
  margin: 0 0 20px;
  font-size: 20px;
`;

CreateEmployeeModal.Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

CreateEmployeeModal.ErrorBox = styled.p`
  position: absolute;
  width: 100px;
  padding: 5px 0;
  text-align: center;
  margin: 0;
  box-sizing: border-box;
  right: 40px;
  top: 20px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateEmployeeModal;
