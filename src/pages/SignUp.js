import { Button, Tooltip, Typography } from '@material-ui/core';
import { AccountBalance, AccountCircle, Email } from '@material-ui/icons';

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { signUp } from '../actions/session';

import { routePaths } from '../routes';

import { PasswordInput, Input } from '../components';

const SignUp = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [isValidationSuccess, setIsValidationSuccess] = useState(true);
  const [error, setError] = useState('');

  const checkPasswordMatch = () =>
    repeatPassword && password !== repeatPassword;

  const handleSignIn = () => {
    history.push(routePaths.signIn);
  };

  const handleSignUp = async e => {
    e.preventDefault();
    const isPasswordMatch = !checkPasswordMatch();
    setIsValidationSuccess(isPasswordMatch);
    if (isPasswordMatch) {
      try {
        await dispatch(
          signUp({
            payload: {
              name: username,
              password,
              email,
              stores: [],
            },
          }),
        );
        history.push(routePaths.selectStore);
      } catch (err) {
        setError('Неправильні дані');
      }
    }
  };

  return (
    <SignUp.Wrapper>
      <SignUp.Header>
        <Typography variant="h6">Інформаційно-Аналітична Система</Typography>
        <AccountBalance style={{ width: 32, height: 32 }} />
      </SignUp.Header>
      <SignUp.Grouper>
        <SignUp.Title>Реєстрація</SignUp.Title>
        {error && <SignUp.ErrorBox>{error}</SignUp.ErrorBox>}
        <SignUp.Form onSubmit={handleSignUp}>
          <Tooltip
            title="Потрібне унікальне ім'я користувача"
            placement="right"
            leaveDelay={500}
            disableHoverListener
          >
            <Input
              autoFocus
              label="Ім'я"
              value={username}
              onInput={value => setUsername(value)}
              style={{ marginBottom: 20 }}
              icon={AccountCircle}
              required
            />
          </Tooltip>
          <Tooltip
            title="Потрібний унікальний e-mail"
            placement="right"
            leaveDelay={500}
            disableHoverListener
          >
            <Input
              label="E-mail"
              value={email}
              onInput={value => setEmail(value)}
              style={{ marginBottom: 20 }}
              icon={Email}
              required
            />
          </Tooltip>
          <Tooltip
            title="Мінімальна довжина пароля - 6 символів"
            placement="right"
            leaveDelay={500}
            PopperProps={{ style: { marginLeft: 55 } }}
            disableHoverListener
          >
            <PasswordInput
              label="Пароль"
              value={password}
              onInput={value => setPassword(value)}
              style={{ marginBottom: 20 }}
              name={password}
              required
            />
          </Tooltip>
          <PasswordInput
            label="Повторіть пароль"
            value={repeatPassword}
            onInput={value => {
              setIsValidationSuccess(true);
              setRepeatPassword(value);
            }}
            style={{ marginBottom: 30 }}
            name="repeatPassword"
            error={!isValidationSuccess}
          />
          <SignUp.ActionContainer>
            <Button
              color="secondary"
              onClick={handleSignIn}
              style={{ marginRight: 20 }}
            >
              Увійти
            </Button>
            <Button variant="outlined" type="submit">
              Зареєструватися
            </Button>
          </SignUp.ActionContainer>
        </SignUp.Form>
      </SignUp.Grouper>
    </SignUp.Wrapper>
  );
};

SignUp.Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;

SignUp.Header = styled.div`
  position: absolute;
  top: 15px;
  left: 0;
  filter: contrast(0.5);
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 22px;
  justify-content: space-between;
  box-sizing: border-box;
`;

SignUp.Grouper = styled.div`
  position: relative;
  width: 320px;
`;

SignUp.Title = styled.h2`
  font-size: 30px;
  margin: 0 0 40px;
`;

SignUp.Form = styled.form`
  display: flex;
  flex-direction: column;
`;

SignUp.ActionContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

SignUp.ErrorBox = styled.p`
  position: absolute;
  width: 100%;
  padding: 5px 10px;
  margin: 0;
  box-sizing: border-box;
  top: 40px;
  left: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default SignUp;
