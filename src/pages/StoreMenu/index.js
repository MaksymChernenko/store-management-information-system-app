/* eslint-disable no-underscore-dangle */
import { CircularProgress } from '@material-ui/core';

import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import update from 'immutability-helper';
import styled from 'styled-components';

import * as menuConfigActions from '../../actions/menuConfig';
import * as menuConfigSelectors from '../../selectors/menuConfig';
import * as sessionSelectors from '../../selectors/session';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import {
  storeMenuCards,
  getOnClickStoreCardHandler,
  getStoreCardIcon,
} from '../../utils';

import MenuCard from './MenuCard';

import backgroundImg from '../../assets/store.jpeg';

const StoreMenu = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const storeMenuConfig = useSelector(menuConfigSelectors.getStoreMenuConfig);
  const storeMenuConfigState = useSelector(
    menuConfigSelectors.getStoreMenuConfigState,
  );
  const store = useSelector(storeSelectors.getStore);
  const user = useSelector(sessionSelectors.getUser);

  const [menuCards, setMenuCards] = useState([]);

  const getConfig = async () => {
    const { menuConfig: config } = await dispatch(
      menuConfigActions.fetchMenuConfig({
        storeId: store.id,
        userId: user.id,
        configType: 'storeMenu',
      }),
    );
    if (!config) {
      dispatch(
        menuConfigActions.addMenuConfig({
          configType: 'storeMenu',
          payload: {
            type: 'storeMenu',
            menuItems: storeMenuCards,
            userId: user.id,
            storeId: store.id,
          },
        }),
      );
    }
  };

  useEffect(() => {
    getConfig();
    return () =>
      dispatch(menuConfigActions.clearMenuConfig({ configType: 'storeMenu' }));
  }, []);

  useEffect(() => {
    if (storeMenuConfig && menuCards.length > 0) {
      dispatch(
        menuConfigActions.updateMenuConfig({
          id: storeMenuConfig._id,
          payload: {
            menuItems: menuCards,
          },
        }),
      );
    }
  }, [menuCards]);

  useEffect(() => {
    if (storeMenuConfig) {
      setMenuCards(storeMenuConfig.menuItems);
    }
  }, [storeMenuConfig]);

  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = menuCards[dragIndex];
      setMenuCards(
        update(menuCards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        }),
      );
    },
    [menuCards],
  );

  const renderCard = (
    // eslint-disable-next-line react/prop-types
    { title },
    index,
  ) => {
    if (!history) return null;
    return (
      <MenuCard
        key={title}
        icon={getStoreCardIcon[title]}
        index={index}
        onClick={getOnClickStoreCardHandler(history, id)[title]}
        title={title}
        moveCard={moveCard}
      />
    );
  };

  return (
    <StoreMenu.Wrapper image={backgroundImg}>
      {storeMenuConfigState === FETCHING_STATE.FETCHING && <CircularProgress />}
      <StoreMenu.MenuGrid>
        {menuCards.map((card, i) => renderCard(card, i))}
      </StoreMenu.MenuGrid>
    </StoreMenu.Wrapper>
  );
};

StoreMenu.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 64px);

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-image: ${({ image }) => `url(${image})`};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    filter: contrast(0.3);
  }
`;

StoreMenu.MenuGrid = styled.div`
  width: 744px;
  position: relative;
  display: flex;
  flex-wrap: wrap;
`;

export default StoreMenu;
