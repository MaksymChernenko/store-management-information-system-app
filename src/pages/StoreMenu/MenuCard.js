import PropTypes from 'prop-types';

import React, { useRef } from 'react';
import styled from 'styled-components';
import { useDrag, useDrop } from 'react-dnd';

const MenuCard = ({ icon: Icon, onClick, title, index, moveCard }) => {
  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: 'card',
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveCard(dragIndex, hoverIndex);
      // eslint-disable-next-line no-param-reassign
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    item: { type: 'card', id: title, index },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  return (
    <MenuCard.Wrapper style={{ opacity }} ref={ref} onClick={onClick}>
      <Icon style={{ width: 100, height: 100, marginRight: 20 }} />
      <MenuCard.Title>{title}</MenuCard.Title>
    </MenuCard.Wrapper>
  );
};

MenuCard.propTypes = {
  icon: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  moveCard: PropTypes.func.isRequired,
};

MenuCard.Wrapper = styled.button`
  width: 270px;
  display: flex;
  align-items: center;
  padding: 46px 20px;
  margin: 30px;
  border: none;
  color: #fff;
  background-color: rgba(55, 58, 64, 0.8);
  box-sizing: content-box;
  cursor: pointer;

  :hover {
    transform: scale(1.06);
    background-color: rgba(55, 58, 64, 0.9);
  }

  :focus {
    outline: none;
    transform: scale(1.06);
    background-color: rgba(55, 58, 64, 0.9);
  }
`;

MenuCard.Title = styled.h4`
  margin: 0;
  font-size: 20px;
`;

export default MenuCard;
