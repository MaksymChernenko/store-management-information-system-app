import { Breadcrumbs, Chip } from '@material-ui/core';
import { LocalConvenienceStore, Menu } from '@material-ui/icons';

import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import styled from 'styled-components';

import { StyledBreadcrumb } from '../../utils';

import ConfigMenu from './ConfigMenu';

const StoreLifecycle = () => {
  const history = useHistory();
  const { id } = useParams();

  const handleGoBack = () => history.push(`/app/store/${id}/store-menu`);

  return (
    <StoreLifecycle.Wrapper>
      <Breadcrumbs
        style={{ position: 'absolute', top: 16, left: 16 }}
        aria-label="breadcrumb"
      >
        <StyledBreadcrumb
          component="button"
          label="Меню"
          icon={<Menu fontSize="small" />}
          onClick={handleGoBack}
        />
        <Chip
          label="Життєвий цикл системи"
          style={{ backgroundColor: 'inherit' }}
          icon={<LocalConvenienceStore fontSize="small" />}
        />
      </Breadcrumbs>
      <StoreLifecycle.Text>Життєвий цикл системи</StoreLifecycle.Text>
      <ConfigMenu />
    </StoreLifecycle.Wrapper>
  );
};

StoreLifecycle.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 64px 20px 30px;
`;

StoreLifecycle.Text = styled.p`
  margin: 0 0 30px;
  font-size: 30px;
`;

export default StoreLifecycle;
