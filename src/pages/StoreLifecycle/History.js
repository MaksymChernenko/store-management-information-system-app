import { CircularProgress } from '@material-ui/core';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as actions from '../../actions/history';
import * as historySelectors from '../../selectors/history';
import * as storeSelectors from '../../selectors/store';

import { FETCHING_STATE } from '../../services/reduxHelpers';
import { getElapsedTime } from '../../utils';

const buildColumnDefs = () => [
  { headerName: 'Користувач', field: 'user' },
  { headerName: 'Дія', field: 'action' },
  {
    headerName: 'Часова мітка',
    field: 'timeStamp',
    valueGetter: ({ data: { createdAt } }) => getElapsedTime(createdAt),
  },
];

const History = () => {
  const dispatch = useDispatch();

  const notes = useSelector(historySelectors.getFilteredNotes);
  const notesState = useSelector(historySelectors.getNotesState);
  const store = useSelector(storeSelectors.getStore);

  useEffect(() => {
    dispatch(actions.fetchNotes({ storeId: store.id }));
  }, []);

  return notesState === FETCHING_STATE.FETCHING ? (
    <History.Loader>
      <CircularProgress />
    </History.Loader>
  ) : (
    <History.GridWrapper className="ag-theme-material">
      <AgGridReact
        columnDefs={buildColumnDefs()}
        rowData={notes.slice().reverse()}
        enableFilter
        enableSorting
        enableColResize
        onGridReady={({ api }) => api.sizeColumnsToFit()}
      />
    </History.GridWrapper>
  );
};

History.GridWrapper = styled.div`
  height: 316px;

  & .ag-header-cell {
    background-color: #484c54 !important;
    color: #fff !important;
  }

  & .ag-header-cell-resize {
    background-color: #fff !important;
    width: 5px !important;
    height: 50%;
  }

  & .ag-icon {
    color: #fff !important;
  }
`;

History.Loader = styled.div`
  height: 316px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default History;
