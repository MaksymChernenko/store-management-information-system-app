import PropTypes from 'prop-types';

import { Button, Checkbox, FormControlLabel } from '@material-ui/core';
import { Check, Clear } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as historyActions from '../../actions/history';
import * as sessionActions from '../../actions/session';
import * as historySelectors from '../../selectors/history';
import * as sessionSelectors from '../../selectors/session';

import { Select } from '../../components';

const Settings = ({ value }) => {
  const dispatch = useDispatch();

  const users = useSelector(sessionSelectors.getUsers);
  const filters = useSelector(historySelectors.getFilters);

  const [user, setUser] = useState(null);
  const [byUser, setByUser] = useState(false);

  useEffect(() => {
    dispatch(sessionActions.fetchUsers());
  }, []);

  useEffect(() => {
    const { user: userFilter } = filters;
    if (userFilter) {
      setByUser(true);
      setUser({ label: userFilter, value: userFilter });
    }
  }, [value]);

  const clearFilters = () => {
    dispatch(historyActions.clearNotesFilters());
    setByUser(false);
    setUser(null);
  };

  const submitFilters = e => {
    e.preventDefault();
    dispatch(
      historyActions.setNotesFilters({
        user: byUser && user ? user.value : '',
      }),
    );
  };

  return (
    <Settings.Form onSubmit={submitFilters}>
      <Settings.Text>Фільтри</Settings.Text>
      <Settings.Row>
        <FormControlLabel
          control={
            <Checkbox
              checked={byUser}
              onChange={({ target: { checked } }) => setByUser(checked)}
            />
          }
          labelPlacement="start"
          label="За користувачем:"
        />
        <Settings.SelectWrapper>
          <Select
            isDisabled={!byUser}
            menuWidth={300}
            placeholder="Вибрати"
            value={user}
            onChange={val => setUser(val)}
            options={users.map(v => ({ value: v.name, label: v.name }))}
          />
        </Settings.SelectWrapper>
      </Settings.Row>
      <Settings.Actions>
        <Button
          type="submit"
          style={{ borderRadius: 0 }}
          variant="outlined"
          endIcon={<Check />}
        >
          Підтвердити фільтри
        </Button>
        <Button
          onClick={clearFilters}
          style={{ borderRadius: 0 }}
          color="secondary"
          endIcon={<Clear />}
        >
          Видалити фільтри
        </Button>
      </Settings.Actions>
    </Settings.Form>
  );
};

Settings.propTypes = {
  value: PropTypes.number.isRequired,
};

Settings.Form = styled.form``;

Settings.Row = styled.div`
  display: flex;
  justify-content: space-between;
  width: 518px;
  margin-bottom: 20px;
`;

Settings.Text = styled.p`
  margin: 0 0 20px;
  padding-bottom: 10px;
  border-bottom: 1px solid;
  font-size: 18px;
`;

Settings.SelectWrapper = styled.div`
  width: 300;
`;

Settings.Actions = styled.div`
  width: 420px;
  display: flex;
  justify-content: space-between;
  margin: 0 20px 0 auto;
`;

export default Settings;
