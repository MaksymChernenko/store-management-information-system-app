import { Tab, Tabs } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { History, SettingsApplications } from '@material-ui/icons';

import React, { useState } from 'react';

import { a11yTabProps } from '../../utils';

import TabPanel from '../../components/TabPanel';

import HistoryTab from './History';
import SettingsTab from './SettingsTab';

const useStyles = makeStyles(theme => ({
  root: {
    boxShadow: '0px 0px 1px',
    backgroundColor: theme.palette.background.paper,
    height: 440,
    width: '100%',
  },
  tabs: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    '& .MuiTab-textColorInherit.Mui-selected': {
      backgroundColor: '#edf4ff',
      filter: 'grayscale(0.6)',
    },
    '& .PrivateTabIndicator-colorSecondary-203': {
      backgroundColor: '#00acf5',
    },
  },
  tab: {
    '& .MuiTab-wrapper': {
      flexDirection: 'row',
    },
    '& .MuiSvgIcon-root': {
      margin: '4px 10px 0 0',
    },
  },
}));

const ConfigMenu = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="Horizontal tabs"
        className={classes.tabs}
      >
        <Tab
          className={classes.tab}
          icon={<History />}
          label="Історія"
          {...a11yTabProps(0)}
        />
        <Tab
          className={classes.tab}
          icon={<SettingsApplications />}
          label="Налаштування"
          {...a11yTabProps(1)}
        />
      </Tabs>
      <TabPanel value={value} index={0}>
        <HistoryTab />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <SettingsTab value={value} />
      </TabPanel>
    </div>
  );
};

export default ConfigMenu;
