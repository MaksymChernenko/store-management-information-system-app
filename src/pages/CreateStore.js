import { Breadcrumbs, Button, Chip } from '@material-ui/core';
import { AddCircleOutline, Home, Store } from '@material-ui/icons';

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import * as sessionActions from '../actions/session';
import * as storeActions from '../actions/store';
import * as sessionSelectors from '../selectors/session';

import { routePaths } from '../routes';
import { StyledBreadcrumb } from '../utils';

import { Input } from '../components';

const CreateStore = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const user = useSelector(sessionSelectors.getUser);

  const [storeName, setStoreName] = useState('');
  const [error, setError] = useState('');

  const handleGoBack = () => history.push(routePaths.selectStore);

  const handleCreateStore = async e => {
    e.preventDefault();
    const userId = user.id;
    try {
      const {
        store: { _id },
      } = await dispatch(
        storeActions.createStore({
          payload: { name: storeName, creator: userId },
        }),
      );
      await dispatch(
        sessionActions.addStoreToUser({
          payload: { storeId: _id },
          id: userId,
        }),
      );
      history.push(`/app/store/${_id}/store-menu`);
    } catch (err) {
      setError('Помилка');
    }
  };

  return (
    <CreateStore.Wrapper>
      <Breadcrumbs
        style={{ position: 'absolute', top: 16, left: 16 }}
        aria-label="breadcrumb"
      >
        <StyledBreadcrumb
          component="button"
          label="Вибір магазину"
          icon={<Home fontSize="small" />}
          onClick={handleGoBack}
        />
        <Chip
          label="Створити магазин"
          style={{ backgroundColor: 'inherit' }}
          icon={<AddCircleOutline fontSize="small" />}
        />
      </Breadcrumbs>
      <CreateStore.Text>Створити магазин</CreateStore.Text>
      <CreateStore.Form onSubmit={handleCreateStore}>
        <Input
          autoFocus
          label="Назва магазину"
          value={storeName}
          onInput={value => setStoreName(value)}
          style={{ marginRight: 20 }}
          icon={Store}
        />
        <Button variant="outlined" type="submit">
          Створити
        </Button>
        {error && <CreateStore.ErrorBox>{error}</CreateStore.ErrorBox>}
      </CreateStore.Form>
    </CreateStore.Wrapper>
  );
};

CreateStore.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 64px);
`;

CreateStore.Text = styled.p`
  margin: 0 0 30px;
  font-size: 30px;
`;

CreateStore.Form = styled.form`
  position: relative;
  display: flex;
  align-items: flex-end;
`;

CreateStore.ErrorBox = styled.p`
  position: absolute;
  width: 100%;
  padding: 5px 10px;
  margin: 0;
  box-sizing: border-box;
  bottom: -40px;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default CreateStore;
