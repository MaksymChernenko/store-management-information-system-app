import { Button, Typography } from '@material-ui/core';
import { AccountBalance, AccountCircle } from '@material-ui/icons';

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { signIn } from '../actions/session';

import { routePaths } from '../routes';

import { PasswordInput, Input } from '../components';

const SignIn = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);

  const handleSignIn = async e => {
    e.preventDefault();
    try {
      await dispatch(
        signIn({
          payload: {
            name,
            password,
          },
        }),
      );
      history.push(routePaths.selectStore);
    } catch (err) {
      setError(true);
    }
  };

  const handleSignUp = () => {
    history.push(routePaths.signUp);
  };

  return (
    <SignIn.Wrapper>
      <SignIn.Header>
        <Typography variant="h6">Інформаційно-Аналітична Система</Typography>
        <AccountBalance style={{ width: 32, height: 32 }} />
      </SignIn.Header>
      <SignIn.Grouper>
        <SignIn.Title>Вхід</SignIn.Title>
        {error && (
          <SignIn.ErrorBox>Неправильне ім&apos;я чи пароль</SignIn.ErrorBox>
        )}
        <SignIn.Form error={error} onSubmit={handleSignIn}>
          <Input
            autoFocus
            label="Ім'я"
            value={name}
            onInput={value => setName(value)}
            style={{ marginBottom: 20 }}
            icon={AccountCircle}
            required
          />
          <PasswordInput
            label="Пароль"
            value={password}
            onInput={value => setPassword(value)}
            style={{ marginBottom: 30 }}
            name="password"
            required
          />
          <SignIn.ActionContainer>
            <Button
              color="secondary"
              onClick={handleSignUp}
              style={{ marginRight: 20 }}
            >
              Реєстрація
            </Button>
            <Button variant="outlined" type="submit">
              Підтвердити
            </Button>
          </SignIn.ActionContainer>
        </SignIn.Form>
      </SignIn.Grouper>
    </SignIn.Wrapper>
  );
};

SignIn.Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;

SignIn.Header = styled.div`
  position: absolute;
  top: 15px;
  left: 0;
  filter: contrast(0.5);
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 22px;
  justify-content: space-between;
  box-sizing: border-box;
`;

SignIn.Grouper = styled.div`
  position: relative;
  width: 320px;
`;

SignIn.Title = styled.h2`
  font-size: 30px;
  margin: 0 0 40px;
`;

SignIn.Form = styled.form`
  display: flex;
  flex-direction: column;
`;

SignIn.ActionContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

SignIn.ErrorBox = styled.p`
  position: absolute;
  width: 100%;
  padding: 5px 10px;
  margin: 0;
  box-sizing: border-box;
  top: 40px;
  left: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default SignIn;
