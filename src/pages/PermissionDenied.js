import { Button } from '@material-ui/core';
import { Block } from '@material-ui/icons';

import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { routePaths } from '../routes';

const PermissionDenied = () => {
  const history = useHistory();

  const handleGoToSelectPage = () => history.push(routePaths.selectStore);

  return (
    <PermissionDenied.Wrapper>
      <PermissionDenied.Title>Дозвіл відхилено</PermissionDenied.Title>
      <Block style={{ width: 300, height: 300 }} />
      <Button
        style={{ borderRadius: 0 }}
        variant="outlined"
        onClick={handleGoToSelectPage}
        color="secondary"
      >
        Повернутися до сторінки вибору магазину
      </Button>
    </PermissionDenied.Wrapper>
  );
};

PermissionDenied.Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 64px);
`;

PermissionDenied.Title = styled.h3`
  margin: 0;
  font-size: 24px;
  color: #f50057;
`;

export default PermissionDenied;
