import { Breadcrumbs, Button, Chip, Fab, Tooltip } from '@material-ui/core';
import { Add, Home, Launch } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import * as actions from '../actions/store';
import * as selectors from '../selectors/store';

import { routePaths } from '../routes';

import { Select } from '../components';

const SelectStore = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const stores = useSelector(selectors.getStores);

  const [store, setStore] = useState(null);

  useEffect(() => {
    dispatch(actions.fetchStores());
  }, []);

  const handleSelectStore = () =>
    history.push(`/app/store/${store.value}/store-menu`);

  const handleCreateStore = () => history.push(routePaths.createStore);

  return (
    <SelectStore.Wrapper>
      <Breadcrumbs
        style={{ position: 'absolute', top: 16, left: 16 }}
        aria-label="breadcrumb"
      >
        <Chip
          label="Вибір магазину"
          style={{ backgroundColor: 'inherit' }}
          icon={<Home fontSize="small" />}
        />
      </Breadcrumbs>
      <SelectStore.Text>Виберіть магазин</SelectStore.Text>
      <div style={{ display: 'flex', marginBottom: 30 }}>
        <div style={{ width: 270, marginRight: 30 }}>
          <Select
            autoFocus
            placeholder="Вибрати"
            width={270}
            value={store}
            onChange={value => setStore(value)}
            // eslint-disable-next-line no-underscore-dangle
            options={stores.map(val => ({ label: val.name, value: val._id }))}
          />
        </div>
        <Button
          onClick={handleSelectStore}
          style={{ borderRadius: 0 }}
          variant="outlined"
          disabled={!store}
          endIcon={<Launch />}
        >
          Вибрати
        </Button>
      </div>
      <SelectStore.Text>Або</SelectStore.Text>
      <SelectStore.Text>Створіть новий</SelectStore.Text>
      <Tooltip title="Створити магазин" aria-label="create store">
        <Fab
          onClick={handleCreateStore}
          style={{ backgroundColor: '#373a40', color: '#fff' }}
          aria-label="add"
        >
          <Add />
        </Fab>
      </Tooltip>
    </SelectStore.Wrapper>
  );
};

SelectStore.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 64px);
`;

SelectStore.Text = styled.p`
  margin: 0 0 30px;
  font-size: 30px;
`;

export default SelectStore;
