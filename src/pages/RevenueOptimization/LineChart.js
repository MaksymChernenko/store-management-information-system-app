import PropTypes from 'prop-types';

import React from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
} from 'recharts';

const CustomTooltip = ({ active, payload }) => {
  if (active) {
    const { price, pricePct, volume, volumePct } = payload[0].payload;
    return (
      <div
        style={{
          padding: '5px 10px',
          backgroundColor: '#373a40',
          color: '#fff',
        }}
      >
        <p>{`Ціна: ${price} грн.`}</p>
        <p>{`Зміна ціни: ${pricePct}`}</p>
        <p>{`Об'єм продаж: ${volume} шт.`}</p>
        <p>{`Зміна об'єму продаж: ${volumePct}`}</p>
      </div>
    );
  }

  return null;
};

CustomTooltip.propTypes = {
  active: PropTypes.bool.isRequired,
  payload: PropTypes.array.isRequired,
};

const Chart = ({ data }) => (
  <LineChart
    style={{ paddingBottom: 10 }}
    width={1000}
    height={400}
    data={data}
    margin={{ top: 5, right: 30, left: 20, bottom: 20 }}
  >
    <XAxis
      dataKey="price"
      label={{ value: 'Ціна (грн)', angle: 0, position: 'bottom' }}
    />
    <YAxis
      label={{ value: `Об'єм продаж (шт)`, angle: -90, position: 'left' }}
    />
    <CartesianGrid strokeDasharray="3 3" />
    <Tooltip content={<CustomTooltip />} />
    <Line type="monotone" dataKey="volume" stroke="#000" />
  </LineChart>
);

Chart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Chart;
