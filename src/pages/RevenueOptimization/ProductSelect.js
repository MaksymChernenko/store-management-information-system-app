/* eslint-disable no-underscore-dangle */
import PropTypes from 'prop-types';

import { Button, LinearProgress } from '@material-ui/core';
import { LocalOffer, MonetizationOn } from '@material-ui/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as optimizationActions from '../../actions/optimization';
import * as productActions from '../../actions/product';
import * as productSelectors from '../../selectors/product';
import * as storeSelectors from '../../selectors/store';

import { Input, Select } from '../../components';

const ProductSelect = ({ handleNext, product, setProduct }) => {
  const dispatch = useDispatch();

  const products = useSelector(productSelectors.getProducts);
  const store = useSelector(storeSelectors.getStore);

  const [changePriceTo, setChangePriceTo] = useState('');
  const [fixedCosts, setFixedCosts] = useState('');

  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    if (products.length === 0) {
      dispatch(productActions.fetchProducts({ storeId: store.id }));
    }
  }, []);

  const calculateRevenue = async () => {
    setIsError(false);
    try {
      setIsLoading(true);
      await dispatch(
        optimizationActions.calculateProductProfit({
          productId: product.value,
          changePriceTo,
          fixedCosts,
        }),
      );
      setIsLoading(false);
      handleNext();
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
    }
  };

  return (
    <>
      <ProductSelect.Action>
        <Button disabled style={{ marginRight: 16 }}>
          Назад
        </Button>
        <Button
          variant="outlined"
          disabled={!product || !+changePriceTo || !+fixedCosts}
          onClick={calculateRevenue}
        >
          Розрахувати
        </Button>
      </ProductSelect.Action>
      <ProductSelect.ContentWrapper>
        <ProductSelect.SelectWrapper>
          <Select
            autoFocus
            placeholder="Вибрати товар"
            value={product}
            width={260}
            menuWidth={260}
            onChange={val => {
              setProduct(val);
            }}
            options={products.map(val => ({
              label: `${val.name} (${val._id})`,
              value: val._id,
            }))}
          />
        </ProductSelect.SelectWrapper>
        <Input
          label="Змінити ціну на"
          type="number"
          value={changePriceTo}
          onInput={value => setChangePriceTo(value)}
          style={{ width: 120, marginRight: 20 }}
          icon={LocalOffer}
        />
        <Input
          label="Постійні витрати"
          type="number"
          value={fixedCosts}
          onInput={value => setFixedCosts(value)}
          style={{ width: 140 }}
          icon={MonetizationOn}
        />
      </ProductSelect.ContentWrapper>
      <ProductSelect.Status>
        {isLoading && (
          <LinearProgress color="secondary" style={{ width: '100%' }} />
        )}
        {isError && <ProductSelect.ErrorBox>Помилка !</ProductSelect.ErrorBox>}
      </ProductSelect.Status>
    </>
  );
};

ProductSelect.propTypes = {
  handleNext: PropTypes.func.isRequired,
  product: PropTypes.object.isRequired,
  setProduct: PropTypes.func.isRequired,
};

ProductSelect.Action = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
`;

ProductSelect.ContentWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin-bottom: 60px;
`;

ProductSelect.SelectWrapper = styled.div`
  width: 260px;
  margin-right: 40px;
`;

ProductSelect.Status = styled.div`
  width: 300px;
  margin: 0 auto;
`;

ProductSelect.ErrorBox = styled.p`
  text-align: center;
  padding: 5px 10px;
  margin: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default ProductSelect;
