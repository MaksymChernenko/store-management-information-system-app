import { Button, Step, StepLabel, Stepper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import React, { useState } from 'react';
import styled from 'styled-components';

import ProductSelect from './ProductSelect';
import ProfitRetentionCurve from './ProfitRetentionCurve';
import RevenueCalculation from './RevenueCalculation';

import { ColorlibStepperConnector, ColorlibStepIcon } from '../../components';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
  },
  stepper: {
    backgroundColor: 'inherit',
  },
}));

const getSteps = () => [
  'Вибір товару',
  'Прибуток по товару',
  'Крива збереження рівня прибутку',
];

const getStepContent = ({
  activeStep,
  handleBack,
  handleNext,
  product,
  setProduct,
}) => {
  switch (activeStep) {
    case 0:
      return (
        <ProductSelect
          handleNext={handleNext}
          product={product}
          setProduct={setProduct}
        />
      );
    case 1:
      return (
        <RevenueCalculation
          handleBack={handleBack}
          handleNext={handleNext}
          product={product}
        />
      );
    case 2:
      return (
        <ProfitRetentionCurve handleBack={handleBack} handleNext={handleNext} />
      );
    default:
      return 'Unknown step';
  }
};

const CustomStepper = () => {
  const classes = useStyles();

  const [product, setProduct] = useState('');

  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={activeStep}
        className={classes.stepper}
        connector={<ColorlibStepperConnector />}
      >
        {steps.map(label => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <CustomStepper.Content>
        {activeStep === steps.length ? (
          <Button
            style={{ display: 'block', margin: '0 auto' }}
            variant="outlined"
            onClick={handleReset}
          >
            Повторити
          </Button>
        ) : (
          getStepContent({
            activeStep,
            handleBack,
            handleNext,
            product,
            setProduct,
          })
        )}
      </CustomStepper.Content>
    </div>
  );
};

CustomStepper.Content = styled.div`
  padding: 10px 20px;
`;

export default CustomStepper;
