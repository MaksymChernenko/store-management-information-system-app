import PropTypes from 'prop-types';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';

import { Button, LinearProgress, Typography } from '@material-ui/core';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import * as optimizationActions from '../../actions/optimization';
import * as optimizationSelectors from '../../selectors/optimization';

import {
  getNecessaryVolumeNaturalValueCount,
  getNecessaryVolumeNaturalValuePct,
  getProductProfitData,
  getTargetPriceChangeNaturalValueCount,
} from '../../utils';

const buildColumnDefs = () => [
  { headerName: '', field: 'title' },
  { headerName: 'Початковий', field: 'source' },
  { headerName: 'Розрахунковий', field: 'estimated' },
];

const RevenueCalculation = ({ handleBack, handleNext, product }) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const productProfit = useSelector(optimizationSelectors.getProductProfit);

  useEffect(() => {
    const main = document.getElementById('main');
    main.scrollTop = main.scrollHeight;
  }, []);

  const buildProfitRetentionCurve = async () => {
    setIsError(false);
    try {
      setIsLoading(true);
      await dispatch(
        optimizationActions.buildProfitRetentionCurve({
          productId: product.value,
        }),
      );
      setIsLoading(false);
      handleNext();
    } catch (error) {
      setIsLoading(false);
      setIsError(true);
    }
  };

  return (
    <>
      <RevenueCalculation.Action>
        <Button
          disabled={isLoading}
          style={{ marginRight: 16 }}
          onClick={handleBack}
        >
          Назад
        </Button>
        <Button variant="outlined" onClick={buildProfitRetentionCurve}>
          Побудувати
        </Button>
      </RevenueCalculation.Action>
      {productProfit && (
        <RevenueCalculation.ContentWrapper>
          <Typography
            style={{ width: 600, textAlign: 'center', marginBottom: 20 }}
            variant="body1"
          >
            {`Для збереження рівня прибутку при ${
              productProfit.targetPriceChange > 0 ? 'збільшенні' : 'зниженні'
            } ціни на ${getTargetPriceChangeNaturalValueCount(
              productProfit,
            )} грн
          потрібно ${
            productProfit.necessaryVolume > 0 ? 'збільшити' : 'зменшити'
          } обсяг продажів на ${getNecessaryVolumeNaturalValuePct(
              productProfit,
            )} % (${getNecessaryVolumeNaturalValueCount(productProfit)} шт.)`}
          </Typography>
          <RevenueCalculation.GridWrapper className="ag-theme-material">
            <AgGridReact
              columnDefs={buildColumnDefs()}
              rowData={getProductProfitData(productProfit)}
              onGridReady={({ api }) => api.sizeColumnsToFit()}
            />
          </RevenueCalculation.GridWrapper>
        </RevenueCalculation.ContentWrapper>
      )}
      <RevenueCalculation.Status>
        {isLoading && (
          <LinearProgress color="secondary" style={{ width: '100%' }} />
        )}
        {isError && (
          <RevenueCalculation.ErrorBox>Помилка !</RevenueCalculation.ErrorBox>
        )}
      </RevenueCalculation.Status>
    </>
  );
};

RevenueCalculation.propTypes = {
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
  product: PropTypes.object.isRequired,
};

RevenueCalculation.Action = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
`;

RevenueCalculation.ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 60px;
`;

RevenueCalculation.GridWrapper = styled.div`
  height: 297px;
  width: 800px;

  & .ag-header-cell {
    background-color: #484c54 !important;
    color: #fff !important;
  }

  & .ag-header-cell-resize {
    background-color: #fff !important;
    width: 5px !important;
    height: 50%;
  }

  & .ag-icon {
    color: #fff !important;
  }
`;

RevenueCalculation.Status = styled.div`
  width: 300px;
  margin: 0 auto;
`;

RevenueCalculation.ErrorBox = styled.p`
  text-align: center;
  padding: 5px 10px;
  margin: 0;
  background-color: #fbe5e1;
  color: #a62d19;
`;

export default RevenueCalculation;
