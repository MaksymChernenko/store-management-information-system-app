import PropTypes from 'prop-types';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-fresh.css';

import { Button, Typography } from '@material-ui/core';

import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import * as optimizationSelectors from '../../selectors/optimization';

import LineChart from './LineChart';

const cellDefaults = {
  minWidth: 90,
  maxWidth: 90,
  cellStyle: { lineHeight: '40px' },
};

const buildColumnDefs = () => [
  {
    field: 'title',
    minWidth: 200,
    cellStyle: { lineHeight: '40px' },
  },
  { field: '-25', ...cellDefaults },
  { field: '-20', ...cellDefaults },
  { field: '-15', ...cellDefaults },
  { field: '-10', ...cellDefaults },
  { field: '-5', ...cellDefaults },
  { field: '0', ...cellDefaults },
  { field: '5', ...cellDefaults },
  { field: '10', ...cellDefaults },
  { field: '15', ...cellDefaults },
  { field: '20', ...cellDefaults },
  { field: '25', ...cellDefaults },
];

const getCurveData = profitRetentionCurveData => {
  const result = Object.values(
    profitRetentionCurveData
      .map(data => {
        const { title: _, ...rest } = data;
        return rest;
      })
      .reduce((acc, item, idx) => {
        Object.entries(item).forEach(([key, value]) => {
          acc[key] = { ...acc[key], [idx]: value };
        });
        return acc;
      }, {}),
  ).map(obj => ({
    pricePct: obj[0],
    volumePct: obj[1],
    price: obj[2],
    volume: obj[3],
  }));
  const [q, w, e, r, t, y, u, i, o, p, a] = result;
  return [u, i, o, p, a, q, w, e, r, t, y];
};

const ProfitRetentionCurve = ({ handleBack, handleNext }) => {
  const profitRetentionCurveData = useSelector(
    optimizationSelectors.getProfitRetentionCurveData,
  );

  useEffect(() => {
    const main = document.getElementById('main');
    main.scrollTop = main.scrollHeight;
  }, []);

  return (
    <>
      <ProfitRetentionCurve.Action>
        <Button style={{ marginRight: 16 }} onClick={handleBack}>
          Назад
        </Button>
        <Button variant="outlined" onClick={handleNext}>
          Завершити
        </Button>
      </ProfitRetentionCurve.Action>
      <ProfitRetentionCurve.ContentWrapper>
        <Typography
          style={{ width: 600, textAlign: 'center', marginBottom: 20 }}
          variant="body1"
        >
          Дані для розрахунку певного рівня прибутку
        </Typography>
        <ProfitRetentionCurve.GridWrapper className="ag-theme-fresh">
          <AgGridReact
            columnDefs={buildColumnDefs()}
            headerHeight={0}
            rowHeight={48}
            rowStyle={{ lineHeight: 48 }}
            rowData={profitRetentionCurveData}
            onGridReady={({ api }) => api.sizeColumnsToFit()}
          />
        </ProfitRetentionCurve.GridWrapper>
        <Typography
          style={{ width: 600, textAlign: 'center', marginBottom: 20 }}
          variant="body1"
        >
          Крива збереження рівня прибутку
        </Typography>
        <LineChart data={getCurveData(profitRetentionCurveData)} />
      </ProfitRetentionCurve.ContentWrapper>
    </>
  );
};

ProfitRetentionCurve.propTypes = {
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
};

ProfitRetentionCurve.Action = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
`;

ProfitRetentionCurve.ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

ProfitRetentionCurve.GridWrapper = styled.div`
  height: 195px;
  width: 1192px;
  margin-bottom: 40px;
`;

export default ProfitRetentionCurve;
