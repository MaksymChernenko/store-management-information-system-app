import { Breadcrumbs, Chip } from '@material-ui/core';
import { BubbleChart, Menu } from '@material-ui/icons';

import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import styled from 'styled-components';

import { StyledBreadcrumb } from '../../utils';

import Stepper from './Stepper';

const RevenueOptimization = () => {
  const history = useHistory();
  const { id } = useParams();

  const handleGoBack = () => history.push(`/app/store/${id}/store-menu`);

  return (
    <RevenueOptimization.Wrapper>
      <Breadcrumbs
        style={{ position: 'absolute', top: 16, left: 16 }}
        aria-label="breadcrumb"
      >
        <StyledBreadcrumb
          component="button"
          label="Меню"
          icon={<Menu fontSize="small" />}
          onClick={handleGoBack}
        />
        <Chip
          label="Оптимізація виручки"
          style={{ backgroundColor: 'inherit' }}
          icon={<BubbleChart fontSize="small" />}
        />
      </Breadcrumbs>
      <RevenueOptimization.Text>Оптимізація виручки</RevenueOptimization.Text>
      <Stepper />
    </RevenueOptimization.Wrapper>
  );
};

RevenueOptimization.Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 64px 20px 30px;
`;

RevenueOptimization.Text = styled.p`
  margin: 0 0 30px;
  font-size: 30px;
`;

export default RevenueOptimization;
