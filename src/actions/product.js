import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/product';

const namespace = 'PRODUCT';

export const FETCH_PRODUCTS = createRequestTypes(`${namespace}/FETCH_PRODUCTS`);

export const fetchProducts = makeApiActionCreator(
  api.fetchProducts,
  FETCH_PRODUCTS[START],
  FETCH_PRODUCTS[SUCCESS],
  FETCH_PRODUCTS[ERROR],
);

export const CREATE_PRODUCT = createRequestTypes(`${namespace}/CREATE_PRODUCT`);

export const createProduct = makeApiActionCreator(
  api.createProduct,
  CREATE_PRODUCT[START],
  CREATE_PRODUCT[SUCCESS],
  CREATE_PRODUCT[ERROR],
);

export const UPDATE_PRODUCT = createRequestTypes(`${namespace}/UPDATE_PRODUCT`);

export const updateProduct = makeApiActionCreator(
  api.updateProduct,
  UPDATE_PRODUCT[START],
  UPDATE_PRODUCT[SUCCESS],
  UPDATE_PRODUCT[ERROR],
);

export const DELETE_PRODUCT = createRequestTypes(`${namespace}/DELETE_PRODUCT`);

export const deleteProduct = makeApiActionCreator(
  api.deleteProduct,
  DELETE_PRODUCT[START],
  DELETE_PRODUCT[SUCCESS],
  DELETE_PRODUCT[ERROR],
);

export const RESET_PRODUCTS = `${namespace}/RESET_PRODUCTS`;

export const resetProducts = () => ({ type: RESET_PRODUCTS });
