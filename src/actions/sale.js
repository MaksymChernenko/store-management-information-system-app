import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/sale';

const namespace = 'SALE';

export const FETCH_SALES = createRequestTypes(`${namespace}/FETCH_SALES`);

export const fetchSales = makeApiActionCreator(
  api.fetchSales,
  FETCH_SALES[START],
  FETCH_SALES[SUCCESS],
  FETCH_SALES[ERROR],
);

export const CREATE_SALE = createRequestTypes(`${namespace}/CREATE_SALE`);

export const createSale = makeApiActionCreator(
  api.createSale,
  CREATE_SALE[START],
  CREATE_SALE[SUCCESS],
  CREATE_SALE[ERROR],
);

export const UPDATE_SALE = createRequestTypes(`${namespace}/UPDATE_SALE`);

export const updateSale = makeApiActionCreator(
  api.updateSale,
  UPDATE_SALE[START],
  UPDATE_SALE[SUCCESS],
  UPDATE_SALE[ERROR],
);

export const DELETE_SALE = createRequestTypes(`${namespace}/DELETE_SALE`);

export const deleteSale = makeApiActionCreator(
  api.deleteSale,
  DELETE_SALE[START],
  DELETE_SALE[SUCCESS],
  DELETE_SALE[ERROR],
);

export const RESET_SALES = `${namespace}/RESET_SALES`;

export const resetSales = () => ({ type: RESET_SALES });
