import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/history';

const namespace = 'HISTORY';

export const FETCH_NOTES = createRequestTypes(`${namespace}/FETCH_NOTES`);

export const fetchNotes = makeApiActionCreator(
  api.fetchNotes,
  FETCH_NOTES[START],
  FETCH_NOTES[SUCCESS],
  FETCH_NOTES[ERROR],
);

export const ADD_NOTE = createRequestTypes(`${namespace}/ADD_NOTE`);

export const addNote = makeApiActionCreator(
  api.addNote,
  ADD_NOTE[START],
  ADD_NOTE[SUCCESS],
  ADD_NOTE[ERROR],
);

export const SET_NOTES_FILTERS = `${namespace}/SET_NOTES_FILTERS`;

export const setNotesFilters = filters => ({
  type: SET_NOTES_FILTERS,
  payload: filters,
});

export const CLEAR_NOTES_FILTERS = `${namespace}/CLEAR_NOTES_FILTERS`;

export const clearNotesFilters = () => ({
  type: CLEAR_NOTES_FILTERS,
});
