import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/employee';

const namespace = 'EMPLOYEE';

export const FETCH_EMPLOYEES = createRequestTypes(
  `${namespace}/FETCH_EMPLOYEES`,
);

export const fetchEmployees = makeApiActionCreator(
  api.fetchEmployees,
  FETCH_EMPLOYEES[START],
  FETCH_EMPLOYEES[SUCCESS],
  FETCH_EMPLOYEES[ERROR],
);

export const CREATE_EMPLOYEE = createRequestTypes(
  `${namespace}/CREATE_EMPLOYEE`,
);

export const createEmployee = makeApiActionCreator(
  api.createEmployee,
  CREATE_EMPLOYEE[START],
  CREATE_EMPLOYEE[SUCCESS],
  CREATE_EMPLOYEE[ERROR],
);

export const UPDATE_EMPLOYEE = createRequestTypes(
  `${namespace}/UPDATE_EMPLOYEE`,
);

export const updateEmployee = makeApiActionCreator(
  api.updateEmployee,
  UPDATE_EMPLOYEE[START],
  UPDATE_EMPLOYEE[SUCCESS],
  UPDATE_EMPLOYEE[ERROR],
);

export const DELETE_EMPLOYEE = createRequestTypes(
  `${namespace}/DELETE_EMPLOYEE`,
);

export const deleteEmployee = makeApiActionCreator(
  api.deleteEmployee,
  DELETE_EMPLOYEE[START],
  DELETE_EMPLOYEE[SUCCESS],
  DELETE_EMPLOYEE[ERROR],
);

export const RESET_EMPLOYEES = `${namespace}/RESET_EMPLOYEES`;

export const resetEmployees = () => ({ type: RESET_EMPLOYEES });
