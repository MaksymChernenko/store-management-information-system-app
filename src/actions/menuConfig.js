import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/menuConfig';

const namespace = 'MENU_CONFIG';

export const FETCH_MENU_CONFIG = createRequestTypes(
  `${namespace}/FETCH_MENU_CONFIG`,
);

export const fetchMenuConfig = makeApiActionCreator(
  api.fetchMenuConfig,
  FETCH_MENU_CONFIG[START],
  FETCH_MENU_CONFIG[SUCCESS],
  FETCH_MENU_CONFIG[ERROR],
);

export const ADD_MENU_CONFIG = createRequestTypes(
  `${namespace}/ADD_MENU_CONFIG`,
);

export const addMenuConfig = makeApiActionCreator(
  api.addMenuConfig,
  ADD_MENU_CONFIG[START],
  ADD_MENU_CONFIG[SUCCESS],
  ADD_MENU_CONFIG[ERROR],
);

export const UPDATE_MENU_CONFIG = createRequestTypes(
  `${namespace}/UPDATE_MENU_CONFIG`,
);

export const updateMenuConfig = makeApiActionCreator(
  api.updateMenuConfig,
  UPDATE_MENU_CONFIG[START],
  UPDATE_MENU_CONFIG[SUCCESS],
  UPDATE_MENU_CONFIG[ERROR],
);

export const RESET_MENU_CONFIG = `${namespace}/RESET_MENU_CONFIG`;

export const clearMenuConfig = ({ configType }) => ({
  type: RESET_MENU_CONFIG,
  configType,
});
