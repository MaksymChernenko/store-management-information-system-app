import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/session';

const namespace = 'SESSION';

export const SIGN_UP = createRequestTypes(`${namespace}/SIGN_UP`);

export const signUp = makeApiActionCreator(
  api.signUp,
  SIGN_UP[START],
  SIGN_UP[SUCCESS],
  SIGN_UP[ERROR],
);

export const SIGN_IN = createRequestTypes(`${namespace}/SIGN_IN`);

export const signIn = makeApiActionCreator(
  api.signIn,
  SIGN_IN[START],
  SIGN_IN[SUCCESS],
  SIGN_IN[ERROR],
);

export const SIGN_OUT = createRequestTypes(`${namespace}/SIGN_OUT`);

export const signOut = makeApiActionCreator(
  api.signOut,
  SIGN_OUT[START],
  SIGN_OUT[SUCCESS],
  SIGN_OUT[ERROR],
);

export const REFRESH_USER = createRequestTypes(`${namespace}/REFRESH_USER`);

export const refreshUser = makeApiActionCreator(
  api.refreshUser,
  REFRESH_USER[START],
  REFRESH_USER[SUCCESS],
  REFRESH_USER[ERROR],
);

export const SET_IS_REFRESHED = `${namespace}/SET_IS_REFRESHED`;

export const FETCH_USERS = createRequestTypes(`${namespace}/FETCH_USERS`);

export const fetchUsers = makeApiActionCreator(
  api.fetchUsers,
  FETCH_USERS[START],
  FETCH_USERS[SUCCESS],
  FETCH_USERS[ERROR],
);

export const ADD_STORE_TO_USER = createRequestTypes(
  `${namespace}/ADD_STORE_TO_USER`,
);

export const addStoreToUser = makeApiActionCreator(
  api.addStoreToUser,
  ADD_STORE_TO_USER[START],
  ADD_STORE_TO_USER[SUCCESS],
  ADD_STORE_TO_USER[ERROR],
);

export const DELETE_USER_FROM_STORE = createRequestTypes(
  `${namespace}/DELETE_USER_FROM_STORE`,
);

export const deleteUserFromStore = makeApiActionCreator(
  api.deleteUserFromStore,
  DELETE_USER_FROM_STORE[START],
  DELETE_USER_FROM_STORE[SUCCESS],
  DELETE_USER_FROM_STORE[ERROR],
);
