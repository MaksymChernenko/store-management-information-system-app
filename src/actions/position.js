import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/position';

const namespace = 'POSITION';

export const FETCH_POSITIONS = createRequestTypes(
  `${namespace}/FETCH_POSITIONS`,
);

export const fetchPositions = makeApiActionCreator(
  api.fetchPositions,
  FETCH_POSITIONS[START],
  FETCH_POSITIONS[SUCCESS],
  FETCH_POSITIONS[ERROR],
);

export const CREATE_POSITION = createRequestTypes(
  `${namespace}/CREATE_POSITION`,
);

export const createPosition = makeApiActionCreator(
  api.createPosition,
  CREATE_POSITION[START],
  CREATE_POSITION[SUCCESS],
  CREATE_POSITION[ERROR],
);

export const UPDATE_POSITION = createRequestTypes(
  `${namespace}/UPDATE_POSITION`,
);

export const updatePosition = makeApiActionCreator(
  api.updatePosition,
  UPDATE_POSITION[START],
  UPDATE_POSITION[SUCCESS],
  UPDATE_POSITION[ERROR],
);

export const DELETE_POSITION = createRequestTypes(
  `${namespace}/DELETE_POSITION`,
);

export const deletePosition = makeApiActionCreator(
  api.deletePosition,
  DELETE_POSITION[START],
  DELETE_POSITION[SUCCESS],
  DELETE_POSITION[ERROR],
);

export const RESET_POSITIONS = `${namespace}/RESET_POSITIONS`;

export const resetPositions = () => ({ type: RESET_POSITIONS });
