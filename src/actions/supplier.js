import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/supplier';

const namespace = 'SUPPLIER';

export const FETCH_SUPPLIERS = createRequestTypes(
  `${namespace}/FETCH_SUPPLIERS`,
);

export const fetchSuppliers = makeApiActionCreator(
  api.fetchSuppliers,
  FETCH_SUPPLIERS[START],
  FETCH_SUPPLIERS[SUCCESS],
  FETCH_SUPPLIERS[ERROR],
);

export const CREATE_SUPPLIER = createRequestTypes(
  `${namespace}/CREATE_SUPPLIER`,
);

export const createSupplier = makeApiActionCreator(
  api.createSupplier,
  CREATE_SUPPLIER[START],
  CREATE_SUPPLIER[SUCCESS],
  CREATE_SUPPLIER[ERROR],
);

export const UPDATE_SUPPLIER = createRequestTypes(
  `${namespace}/UPDATE_SUPPLIER`,
);

export const updateSupplier = makeApiActionCreator(
  api.updateSupplier,
  UPDATE_SUPPLIER[START],
  UPDATE_SUPPLIER[SUCCESS],
  UPDATE_SUPPLIER[ERROR],
);

export const DELETE_SUPPLIER = createRequestTypes(
  `${namespace}/DELETE_SUPPLIER`,
);

export const deleteSupplier = makeApiActionCreator(
  api.deleteSupplier,
  DELETE_SUPPLIER[START],
  DELETE_SUPPLIER[SUCCESS],
  DELETE_SUPPLIER[ERROR],
);

export const RESET_SUPPLIERS = `${namespace}/RESET_SUPPLIERS`;

export const resetSuppliers = () => ({ type: RESET_SUPPLIERS });
