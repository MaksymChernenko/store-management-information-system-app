import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/store';

const namespace = 'STORE';

export const FETCH_STORES = createRequestTypes(`${namespace}/FETCH_STORES`);

export const fetchStores = makeApiActionCreator(
  api.fetchStores,
  FETCH_STORES[START],
  FETCH_STORES[SUCCESS],
  FETCH_STORES[ERROR],
);

export const FETCH_STORE = createRequestTypes(`${namespace}/FETCH_STORE`);

export const fetchStore = makeApiActionCreator(
  api.fetchStore,
  FETCH_STORE[START],
  FETCH_STORE[SUCCESS],
  FETCH_STORE[ERROR],
);

export const CREATE_STORE = createRequestTypes(`${namespace}/CREATE_STORE`);

export const createStore = makeApiActionCreator(
  api.createStore,
  CREATE_STORE[START],
  CREATE_STORE[SUCCESS],
  CREATE_STORE[ERROR],
);

export const SET_ADMIN_RIGHTS = createRequestTypes(
  `${namespace}/SET_ADMIN_RIGHTS`,
);

export const setAdminRights = makeApiActionCreator(
  api.setAdminRights,
  SET_ADMIN_RIGHTS[START],
  SET_ADMIN_RIGHTS[SUCCESS],
  SET_ADMIN_RIGHTS[ERROR],
);

export const DELETE_ADMIN_RIGHTS = createRequestTypes(
  `${namespace}/DELETE_ADMIN_RIGHTS`,
);

export const deleteAdminRights = makeApiActionCreator(
  api.deleteAdminRights,
  DELETE_ADMIN_RIGHTS[START],
  DELETE_ADMIN_RIGHTS[SUCCESS],
  DELETE_ADMIN_RIGHTS[ERROR],
);

export const UPDATE_STORE = createRequestTypes(`${namespace}/UPDATE_STORE`);

export const updateStore = makeApiActionCreator(
  api.updateStore,
  UPDATE_STORE[START],
  UPDATE_STORE[SUCCESS],
  UPDATE_STORE[ERROR],
);

export const DELETE_STORE = createRequestTypes(`${namespace}/DELETE_STORE`);

export const deleteStore = makeApiActionCreator(
  api.deleteStore,
  DELETE_STORE[START],
  DELETE_STORE[SUCCESS],
  DELETE_STORE[ERROR],
);

export const RESET_STORE = `${namespace}/RESET_STORE`;

export const clearStore = () => ({ type: RESET_STORE });
