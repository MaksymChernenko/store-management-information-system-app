import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/analysis';

const namespace = 'ANALYSIS';

export const TRACK_THE_WORK_OF_EMPLOYEES = createRequestTypes(
  `${namespace}/TRACK_THE_WORK_OF_EMPLOYEES`,
);

export const trackTheWorkOfEmployees = makeApiActionCreator(
  api.trackTheWorkOfEmployees,
  TRACK_THE_WORK_OF_EMPLOYEES[START],
  TRACK_THE_WORK_OF_EMPLOYEES[SUCCESS],
  TRACK_THE_WORK_OF_EMPLOYEES[ERROR],
);

export const TRACK_COLLABORATION_WITH_SUPPLIERS = createRequestTypes(
  `${namespace}/TRACK_COLLABORATION_WITH_SUPPLIERS`,
);

export const trackCollaborationWithSuppliers = makeApiActionCreator(
  api.trackCollaborationWithSuppliers,
  TRACK_COLLABORATION_WITH_SUPPLIERS[START],
  TRACK_COLLABORATION_WITH_SUPPLIERS[SUCCESS],
  TRACK_COLLABORATION_WITH_SUPPLIERS[ERROR],
);

export const CHECK_PRODUCTS_RATE = createRequestTypes(
  `${namespace}/CHECK_PRODUCTS_RATE`,
);

export const checkProductsRate = makeApiActionCreator(
  api.checkProductsRate,
  CHECK_PRODUCTS_RATE[START],
  CHECK_PRODUCTS_RATE[SUCCESS],
  CHECK_PRODUCTS_RATE[ERROR],
);
