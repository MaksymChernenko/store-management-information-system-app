import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/waybill';

const namespace = 'WAYBILL';

export const FETCH_WAYBILLS = createRequestTypes(`${namespace}/FETCH_WAYBILLS`);

export const fetchWaybills = makeApiActionCreator(
  api.fetchWaybills,
  FETCH_WAYBILLS[START],
  FETCH_WAYBILLS[SUCCESS],
  FETCH_WAYBILLS[ERROR],
);

export const CREATE_WAYBILL = createRequestTypes(`${namespace}/CREATE_WAYBILL`);

export const createWaybill = makeApiActionCreator(
  api.createWaybill,
  CREATE_WAYBILL[START],
  CREATE_WAYBILL[SUCCESS],
  CREATE_WAYBILL[ERROR],
);

export const UPDATE_WAYBILL = createRequestTypes(`${namespace}/UPDATE_WAYBILL`);

export const updateWaybill = makeApiActionCreator(
  api.updateWaybill,
  UPDATE_WAYBILL[START],
  UPDATE_WAYBILL[SUCCESS],
  UPDATE_WAYBILL[ERROR],
);

export const DELETE_WAYBILL = createRequestTypes(`${namespace}/DELETE_WAYBILL`);

export const deleteWaybill = makeApiActionCreator(
  api.deleteWaybill,
  DELETE_WAYBILL[START],
  DELETE_WAYBILL[SUCCESS],
  DELETE_WAYBILL[ERROR],
);

export const RESET_WAYBILLS = `${namespace}/RESET_WAYBILLS`;

export const resetWaybills = () => ({ type: RESET_WAYBILLS });
