import {
  createRequestTypes,
  makeApiActionCreator,
  START,
  SUCCESS,
  ERROR,
} from '../services/reduxHelpers';

import * as api from '../api/optimization';

const namespace = 'OPTIMIZATION';

export const CALCULATE_PRODUCT_PROFIT = createRequestTypes(
  `${namespace}/CALCULATE_PRODUCT_PROFIT`,
);

export const calculateProductProfit = makeApiActionCreator(
  api.calculateProductProfit,
  CALCULATE_PRODUCT_PROFIT[START],
  CALCULATE_PRODUCT_PROFIT[SUCCESS],
  CALCULATE_PRODUCT_PROFIT[ERROR],
);

export const BUILD_PROFIT_RETENTION_CURVE = createRequestTypes(
  `${namespace}/BUILD_PROFIT_RETENTION_CURVE`,
);

export const buildProfitRetentionCurve = makeApiActionCreator(
  api.buildProfitRetentionCurve,
  BUILD_PROFIT_RETENTION_CURVE[START],
  BUILD_PROFIT_RETENTION_CURVE[SUCCESS],
  BUILD_PROFIT_RETENTION_CURVE[ERROR],
);
