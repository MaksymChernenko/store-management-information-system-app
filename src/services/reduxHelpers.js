export const START = 'START';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';

export function createRequestTypes(base) {
  return [START, SUCCESS, ERROR].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}

const SET_IS_REFRESHED = `SESSION/SET_IS_REFRESHED`;

const setIsRefreshed = value => ({
  type: SET_IS_REFRESHED,
  payload: value,
});

export function makeApiActionCreator(
  _apiCall,
  startActionType,
  successActionType,
  errorActionType,
) {
  return (params, note) => {
    return async (dispatch, getState) => {
      const state = getState();
      const { token } = state.session;
      if (startActionType === 'SESSION/REFRESH_USER_START' && !token) {
        dispatch(setIsRefreshed(true));
        return;
      }

      dispatch({ ...params, type: startActionType });
      try {
        const data = await _apiCall(params);
        dispatch({ ...params, data, type: successActionType, note });
        // eslint-disable-next-line consistent-return
        return data;
      } catch (error) {
        console.log(error); // eslint-disable-line no-console
        dispatch({ ...params, error, type: errorActionType });
        throw error;
      }
    };
  };
}

export const FETCHING_STATE = {
  FETCHING: 'FETCHING',
  LOADED: 'LOADED',
  ERROR: 'ERROR',
};
